/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class ComplementsBienCreate {

    private String CB0000000002_immatriculation;
    private String CB0000000003_poids;
    private String CB0000000004_parcelBookletNumber;
    private String CB0000000005_referenceContrat;
    private String CB0000000006_builtArea;
    private String CB0000000007_openSpaceArea;
    private String CB0000000031_rentAmount;
    private String CB0000000033_annualRentAmount;
    private String CB0000000098_latitude;
    private String CB0000000099_longitude;

    public String getCB0000000002_immatriculation() {
        return CB0000000002_immatriculation;
    }

    public void setCB0000000002_immatriculation(String CB0000000002_immatriculation) {
        this.CB0000000002_immatriculation = CB0000000002_immatriculation;
    }

    public String getCB0000000003_poids() {
        return CB0000000003_poids;
    }

    public void setCB0000000003_poids(String CB0000000003_poids) {
        this.CB0000000003_poids = CB0000000003_poids;
    }

    public String getCB0000000004_parcelBookletNumber() {
        return CB0000000004_parcelBookletNumber;
    }

    public void setCB0000000004_parcelBookletNumber(String CB0000000004_parcelBookletNumber) {
        this.CB0000000004_parcelBookletNumber = CB0000000004_parcelBookletNumber;
    }

    public String getCB0000000005_referenceContrat() {
        return CB0000000005_referenceContrat;
    }

    public void setCB0000000005_referenceContrat(String CB0000000005_referenceContrat) {
        this.CB0000000005_referenceContrat = CB0000000005_referenceContrat;
    }

    public String getCB0000000006_builtArea() {
        return CB0000000006_builtArea;
    }

    public void setCB0000000006_builtArea(String CB0000000006_builtArea) {
        this.CB0000000006_builtArea = CB0000000006_builtArea;
    }

    public String getCB0000000007_openSpaceArea() {
        return CB0000000007_openSpaceArea;
    }

    public void setCB0000000007_openSpaceArea(String CB0000000007_openSpaceArea) {
        this.CB0000000007_openSpaceArea = CB0000000007_openSpaceArea;
    }

    public String getCB0000000031_rentAmount() {
        return CB0000000031_rentAmount;
    }

    public void setCB0000000031_rentAmount(String CB0000000031_rentAmount) {
        this.CB0000000031_rentAmount = CB0000000031_rentAmount;
    }

    public String getCB0000000033_annualRentAmount() {
        return CB0000000033_annualRentAmount;
    }

    public void setCB0000000033_annualRentAmount(String CB0000000033_annualRentAmount) {
        this.CB0000000033_annualRentAmount = CB0000000033_annualRentAmount;
    }

    public String getCB0000000098_latitude() {
        return CB0000000098_latitude;
    }

    public void setCB0000000098_latitude(String CB0000000098_latitude) {
        this.CB0000000098_latitude = CB0000000098_latitude;
    }

    public String getCB0000000099_longitude() {
        return CB0000000099_longitude;
    }

    public void setCB0000000099_longitude(String CB0000000099_longitude) {
        this.CB0000000099_longitude = CB0000000099_longitude;
    }

}
