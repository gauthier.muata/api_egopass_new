/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockPersonCreate {

    private PersonCreate personObject;
    private List<BienCreate> propertyObjectList;

    public MockPersonCreate(PersonCreate personObject, List<BienCreate> propertyObjectList) {
        this.personObject = personObject;
        this.propertyObjectList = propertyObjectList;
    }

    public PersonCreate getPersonObject() {
        return personObject;
    }

    public void setPersonObject(PersonCreate personObject) {
        this.personObject = personObject;
    }

    public List<BienCreate> getPropertyObjectList() {
        return propertyObjectList;
    }

    public void setPropertyObjectList(List<BienCreate> propertyObjectList) {
        this.propertyObjectList = propertyObjectList;
    }

}
