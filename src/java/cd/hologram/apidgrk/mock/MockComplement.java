/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.mock;

/**
 *
 * @author emmanuel.tsasa
 */
public class MockComplement {

    String Color;
    String Horsepower;
    String Weight;
    String Brand;
    String RegistrationNumber;
    String ChassiNumber;

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getHorsepower() {
        return Horsepower;
    }

    public void setHorsepower(String Horsepower) {
        this.Horsepower = Horsepower;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String Weight) {
        this.Weight = Weight;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public void setRegistrationNumber(String RegistrationNumber) {
        this.RegistrationNumber = RegistrationNumber;
    }

    public String getChassiNumber() {
        return ChassiNumber;
    }

    public void setChassiNumber(String ChassiNumber) {
        this.ChassiNumber = ChassiNumber;
    }
}
