/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.mock;

import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class MockDashboardData {

    String locationId;
    String type;
   
    //List<MockPayement> paymentList;

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

//    public List<MockPayement> getPaymentList() {
//        return paymentList;
//    }
//
//    public void setPaymentList(List<MockPayement> paymentList) {
//        this.paymentList = paymentList;
//    }
}
