/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.business;

import cd.hologram.apidgrk.constants.GeneralConst;
import cd.hologram.apidgrk.dao.Dao;
import cd.hologram.apidgrk.entities.*;
import cd.hologram.apidgrk.mock.*;
import cd.hologram.apidgrk.pojo.RDeclaration;
import cd.hologram.apidgrk.utils.Casting;
import cd.hologram.apidgrk.utils.Tools;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author moussa.toure
 */
public class DataAccess implements IDataAccess {

    private static final Dao myDao = new Dao();

    @Override
    public NotePerception getNotePerceptionByNumero(String numero) {

        try {

            String query = "SELECT * FROM T_NOTE_PERCEPTION WITH (READPAST) WHERE NUMERO = ?1";

            List<NotePerception> archives = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, numero);

            return archives.isEmpty() ? null : archives.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Personne getPersonneByCode(String code) {

        try {

            String query = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE CODE = ?1";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, code);

            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Service getServiceByCode(String code) {

        try {

            String query = "SELECT * FROM T_SERVICE WITH (READPAST) WHERE CODE = ?1";

            List<Service> services = (List<Service>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Service.class), false, code);

            return services.isEmpty() ? null : services.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public CompteBancaire getCompteBancaireByCode(String code) {

        try {

            String query = "SELECT * FROM T_COMPTE_BANCAIRE WITH (READPAST) WHERE CODE = ?1";

            List<CompteBancaire> compteBancaires = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, code);

            return compteBancaires.isEmpty() ? null : compteBancaires.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static CompteBancaire getCompteBancaireByCode_V3(String codeCompteBancaire) {
        String query = "SELECT * FROM T_COMPTE_BANCAIRE WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";
        List<CompteBancaire> listCompteBancaire = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, codeCompteBancaire.trim());
        return listCompteBancaire.isEmpty() ? null : listCompteBancaire.get(0);
    }

    public static CompteBancaire getCompteBancaireByCode_V2(String codeCompteBancaire) {
        String query = "SELECT * FROM T_COMPTE_BANCAIRE WITH (READPAST) WHERE CODE = ?1 AND ETAT IN (0,1)";
        List<CompteBancaire> listCompteBancaire = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, codeCompteBancaire.trim());
        return listCompteBancaire.isEmpty() ? null : listCompteBancaire.get(0);
    }

    public static List<CompteBancaire> getCompteBancaireByBanqueCode(String banqueCode) {
        String query = "SELECT * FROM T_COMPTE_BANCAIRE WITH (READPAST) WHERE BANQUE = ?1 AND ETAT = 1";
        List<CompteBancaire> listCompteBancaire = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, banqueCode);
        return listCompteBancaire;
    }

    @Override
    public Amr getAMRByNumero(String numero) {

        try {

            String query = "SELECT * FROM T_AMR WITH (READPAST) WHERE NUMERO = ?1";

            List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Amr.class), false, numero);

            return amrs.isEmpty() ? null : amrs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public Site getSiteByNoteCalcul(String numeroNc) {

        try {

            String query = "SELECT * FROM T_SITE WITH (READPAST) WHERE CODE IN (SELECT NC.SITE FROM T_NOTE_CALCUL NC WITH (READPAST) WHERE NUMERO = ?1) ";

            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false, numeroNc);

            return sites.isEmpty() ? null : sites.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean savePayment(Object... params) {

        boolean result;

        try {

            String query = "EXEC F_NEW_JOURNAL_WEB ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15";

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    params);

        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public boolean logRequest(String client, String url, String response) {

        boolean result;

//        int min = 1;
//        int max = 2000000;
//
//        Random rand = new Random();
//
//        int randomValaue = rand.nextInt((max - min) + 1) + min;
        String LOG_QUERY = "INSERT INTO T_LOG_API (DATE_CREATE,CLIENT,URL,RESPONSE) "
                + " VALUES (GETDATE(),?1,?2,?3)";

        try {
            result = myDao.getDaoImpl().executeNativeQuery(LOG_QUERY,
                    Integer.valueOf(client), url, response);
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public boolean saveDeclaration(Bordereau bordereau, List<DetailBordereau> detailBordereauList) {

        HashMap<String, Object> paramsBD = new HashMap<>();
        HashMap<String, Object[]> bordereauBulk = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;

        String query = ":EXEC F_NEW_DETAIL_BORDEREAU %s,?1,?2,?3,?4";

        String firstStoredProcedure = "F_NEW_BORDEREAU";
        String firstStoredReturnValueKey = "BORDEREAU";

        paramsBD.put("NUMERO_BORDEREAU", bordereau.getNumeroBordereau());
        paramsBD.put("NOM_COMPLET", bordereau.getNomComplet());
        paramsBD.put("DATE_PAIEMENT", bordereau.getDatePaiement());
        paramsBD.put("TOTAL_MONTANT", bordereau.getTotalMontantPercu());
        paramsBD.put("COMPTE_BANCAIRE", bordereau.getCompteBancaire().getCode());
        paramsBD.put("AGENT", bordereau.getAgent().getCode());
        paramsBD.put("DATE_CREATE", bordereau.getDateCreate());
        paramsBD.put("ETAT", bordereau.getEtat().getCode());
        paramsBD.put("NUMERO_DECLARATION", bordereau.getNumeroDeclaration().trim());
        paramsBD.put("NUMERO_ATTESTATION_PAIEMENT", bordereau.getNumeroAttestationPaiement());
        paramsBD.put("CENTRE", bordereau.getCentre());

        for (DetailBordereau detailBordereau : detailBordereauList) {

            bordereauBulk.put(counter
                    + query,
                    new Object[]{
                        detailBordereau.getArticleBudgetaire().getCode(),
                        detailBordereau.getMontantPercu(),
                        detailBordereau.getDevise(),
                        detailBordereau.getPeriodicite()});

            counter++;
        }

        boolean result;

        try {

            String resultString = myDao.getDaoImpl().execBulkQueryv4(firstStoredProcedure, firstStoredReturnValueKey, paramsBD, bordereauBulk);

            result = !resultString.isEmpty();

        } catch (Exception e) {

            result = false;
        }

        return result;
    }

    @Override
    public List<RetraitDeclaration> getRetraitDeclarationByCode(String codeDeclaration) {

        try {

//            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE CODE_DECLARATION = ?1";
//            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE NEW_ID = ?1";
            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE NEW_ID IN("
                    + "SELECT NEW_ID FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1 AND ETAT = 1)";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, codeDeclaration);

            return declarations;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<RetraitDeclaration> getRetraitDeclarationByCodeV2(String codeDeclaration) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1 AND ETAT = 4";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, codeDeclaration);

            return declarations;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<RetraitDeclaration> getRetraitDeclaration(String nif) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE FK_ASSUJETTI = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, nif);

            return declarations;
        } catch (Exception e) {
            throw e;
        }

    }

    @Override
    public ArticleBudgetaire getArticleBudgetairebyCode(String code) {

        try {

            String query = "SELECT * FROM T_ARTICLE_BUDGETAIRE WITH (READPAST) WHERE CODE = ?1";

            List<ArticleBudgetaire> articleBudgetaires = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, code);

            return articleBudgetaires.isEmpty() ? null : articleBudgetaires.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public List<Journal> getJournalByCode(String personne) {
        try {
            String query = "SELECT * FROM T_JOURNAL  WITH (READPAST) WHERE PERSONNE = '%s'";
            query = String.format(query, personne);

            List<Journal> listJournal = (List<Journal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Journal.class), false);

            return listJournal;

        } catch (Exception e) {
            throw e;
        }

    }

    public static ArticleBudgetaire getArticleBudgetaireByCode(String code) {
        try {

            String query = "SELECT * FROM T_ARTICLE_BUDGETAIRE WHERE CODE = ?1 AND ETAT = 1";

            List<ArticleBudgetaire> articleBudgetaireList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, code);

            return articleBudgetaireList == null ? null : articleBudgetaireList.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public PeriodeDeclaration getPeriodeDeclaration(int code) {

        try {

            String query = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ID = ?1";

            List<PeriodeDeclaration> periodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, code);

            return periodeDeclarations.isEmpty() ? null : periodeDeclarations.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<PeriodeDeclaration> getPeriodeByAssujettissement(String assu) {
        try {

            String query = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ASSUJETISSEMENT = ?1 "
                    + "AND ID IN (SELECT FK_PERIODE FROM T_RETRAIT_DECLARATION WHERE ETAT = 1) AND ETAT = 1";

            List<PeriodeDeclaration> periodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, assu);

            return periodeDeclarations;

        } catch (Exception e) {
            throw e;
        }
    }

    public List<RDeclaration> getDeclarationByAssujettissement(String assu) {
        try {

            String query = "SELECT RD.CODE_DECLARATION CODE, PD.* FROM T_PERIODE_DECLARATION PD "
                    + "INNER JOIN T_RETRAIT_DECLARATION RD ON PD.ID = RD.FK_PERIODE "
                    + "WHERE PD.ASSUJETISSEMENT = ?1 AND PD.ETAT = 1 AND RD.ETAT = 1";

            List<RDeclaration> rDeclarations = (List<RDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RDeclaration.class), false, assu);

            return rDeclarations;

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Palier getPalierByTarif(String tarif, String articleBudgetaire) {

        try {

            String query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE TARIF = ?1 AND ARTICLE_BUDGETAIRE = ?2 AND ETAT = 1";

            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, tarif, articleBudgetaire);

            return paliers.isEmpty() ? null : paliers.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static EntiteAdministrative getEntiteAdmByAvenue(String code) {

        try {
            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE CODE = ?1";
            List<EntiteAdministrative> ea = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, code);

            return ea.isEmpty() ? null : ea.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static String createBien(PeriodeDeclaration declaration, RetraitDeclaration retraitDeclaration, String center, String codeAgent, String compteBancaire, String banque) {

        String result = GeneralConst.EMPTY_STRING;

        try {
            result = myDao.getDaoImpl().createBien(declaration, retraitDeclaration, center, codeAgent, compteBancaire, banque);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static String createBien(Assujeti assujeti, String tcb, String tarifCode, String requerant, String amount, String devise, String centre, String codeAgent) {
        String result = GeneralConst.EMPTY_STRING;

        try {
            result = myDao.getDaoImpl().createBien(assujeti, tcb, tarifCode, requerant, amount, devise, centre, codeAgent);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public static ComplementBien getComplementBienImmatriculation(String param, String codeComplement) {

        try {

            String query = "SELECT CB.* FROM T_COMPLEMENT_BIEN CB INNER JOIN T_TYPE_COMPLEMENT_BIEN TCB"
                    + " ON CB.TYPE_COMPLEMENT = TCB.CODE WHERE TCB.COMPLEMENT = ?1"
                    + " AND CB.VALEUR = ?2 AND CB.ETAT = 1 AND TCB.ETAT = 1";

            List<ComplementBien> complementBiens = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, codeComplement, param);

            return (complementBiens.isEmpty()) ? null : complementBiens.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public List<AdressePersonne> getAdressePersonne(String communeId, String quartierId, String avenueId) {

        try {

            String query = "SELECT * FROM T_ADRESSE_PERSONNE WITH (READPAST) WHERE ADRESSE IN("
                    + "SELECT ID FROM T_ADRESSE WITH (READPAST) WHERE AVENUE = ?1 AND QUARTIER = ?2 AND COMMUNE = ?3)";

            List<AdressePersonne> adressePersonnes = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, avenueId, quartierId, communeId);
            return adressePersonnes;

        } catch (Exception e) {
            throw e;
        }
    }

    public static EntiteAdministrative getEntiteAdministrativeByCode(String codeEntite) {
        try {
            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE CODE = ?1";

            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeEntite.trim());

            return entiteAdministratives.isEmpty() ? null : entiteAdministratives.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public EntiteAdministrative getAdministrativeEntities(String code) {

        try {

            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE TYPE_ENTITE = 4 and CODE = ?1";

            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, code);

            return entiteAdministratives.isEmpty() ? null : entiteAdministratives.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static Periodicite getPeriodicite(String codeArticleB) {
        try {
            String query = "SELECT * FROM T_PERIODICITE WITH (READPAST) WHERE CODE IN(SELECT PERIODICITE "
                    + "FROM T_ARTICLE_BUDGETAIRE WITH (READPAST) "
                    + "WHERE ASSUJETISSABLE = 1 AND ETAT = 1 AND CODE = ?1)";

            List<Periodicite> periodicites = (List<Periodicite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Periodicite.class), false, codeArticleB);

            return periodicites.isEmpty() ? null : periodicites.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static NoteCalcul getNC(String codeBien, String datePartParam, String partDate) {
        try {

            String query = "SELECT * FROM T_NOTE_CALCUL WITH (READPAST) WHERE BIEN = ?1 AND NUMERO "
                    + "IN(SELECT NOTE_CALCUL FROM T_DETAILS_NC WITH (READPAST) WHERE PERIODE_DECLARATION "
                    + "IN(SELECT ID FROM T_PERIODE_DECLARATION pd WITH (READPAST) WHERE "
                    + "DATEPART(" + partDate + ",convert(date,'%s')) >= DATEPART(" + partDate + ",convert(date,pd.DEBUT)) "
                    + "AND DATEPART(" + partDate + ",convert(date,'%s'))<= DATEPART(" + partDate + ",convert(date,pd.FIN))))";

            query = String.format(query, datePartParam, datePartParam);

            List<NoteCalcul> noteCalculs = (List<NoteCalcul>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NoteCalcul.class), false, codeBien);

            return noteCalculs.isEmpty() ? null : noteCalculs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArticleBudgetaire> getArticlesBudgetaires() {
        try {

            String query = "SELECT * FROM T_ARTICLE_BUDGETAIRE WITH (READPAST) WHERE ASSUJETISSABLE = 1 AND ETAT = 1 ORDER BY INTITULE";

            List<ArticleBudgetaire> abs = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false);

            return abs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Unite> getUnits() {
        try {

            String query = "SELECT * FROM T_UNITE WITH (READPAST) WHERE ETAT = 1";

            List<Unite> unites = (List<Unite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Unite.class), false);

            return unites;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Devise> getDevise() {
        try {

            String query = "SELECT * FROM T_DEVISE WITH (READPAST) WHERE ETAT = 1";

            List<Devise> ds = (List<Devise>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Devise.class), false);

            return ds;
        } catch (Exception e) {
            throw e;
        }
    }

    public static String savePersonWithProperties(List<MockPersonCreate> mockPersonCreateList) {

        String result = "";

        try {
            result = myDao.getDaoImpl().savePersonAndProperties(mockPersonCreateList);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public static String savePerson(List<PersonCreate> mockPersonCreateList) throws Exception {

        String result = "";

        try {
            result = myDao.getDaoImpl().savePerson(mockPersonCreateList);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public static List<FormeJuridique> getTypePersonne() {
        try {

            String query = "SELECT * FROM T_FORME_JURIDIQUE WITH (READPAST) WHERE ETAT = 1 AND VISIBLE_UTILISATEUR = 1";

            List<FormeJuridique> fjs = (List<FormeJuridique>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(FormeJuridique.class), false);

            return fjs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static String saveBien(List<BienCreate> mockBienCreateList) {
        String result = "";

        try {
            result = myDao.getDaoImpl().saveBien(mockBienCreateList);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    public static List<TypeBien> getTypeBien() {
        try {

            String query = "SELECT * FROM T_TYPE_BIEN WITH (READPAST) WHERE ETAT = 1";

            List<TypeBien> tbs = (List<TypeBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBien.class), false);

            return tbs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Personne> getInformationPersonne(String param) {
        try {

            String query = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE NOM LIKE ?1 OR POSTNOM LIKE ?2 OR PRENOMS LIKE ?3";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, param);

            return personnes;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Personne> getPersonneById(String id) {
        try {

            String query = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE CODE = ?1";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, id);

            return personnes;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ComplementBien> getComplementBien(String codeBien) {
        try {

            String query = "SELECT * FROM T_COMPLEMENT_BIEN WITH (READPAST) WHERE BIEN = ?1";

            List<ComplementBien> cbs = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, codeBien);

            return cbs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<CompteBancaire> getBankAccount(String codeBanque) {
        try {

            String query = "SELECT * FROM T_COMPTE_BANCAIRE WITH (READPAST) WHERE BANQUE = ?1 AND ETAT = 1";

            List<CompteBancaire> cbs = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, codeBanque);

            return cbs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static String saveRetraitDeclaration(List<MockRetraitDeclaration> mockRetraitDeclarCreateList) throws JSONException {

        String response = GeneralConst.EMPTY_STRING;
        JSONObject resultJson = new JSONObject();
        HashMap<String, Object> bulkRetrDecl = new HashMap<>();

        try {

            for (MockRetraitDeclaration mockRetraitDeclarCreate : mockRetraitDeclarCreateList) {

                bulkRetrDecl.put("REQUERANT", mockRetraitDeclarCreate.getRequerant());
                bulkRetrDecl.put("FK_ASSUJETTI", mockRetraitDeclarCreate.getFkAssujetti());
                bulkRetrDecl.put("FK_AB", mockRetraitDeclarCreate.getFkArticleBudgetaire());
                bulkRetrDecl.put("FK_PERIODICITE", mockRetraitDeclarCreate.getFkPeriode());
                bulkRetrDecl.put("FK_AGENT_CREAT", mockRetraitDeclarCreate.getFkAgent());
                bulkRetrDecl.put("VALEUR_BASE", mockRetraitDeclarCreate.getValeurBase());
                bulkRetrDecl.put("MONTANT", mockRetraitDeclarCreate.getAmount());
                bulkRetrDecl.put("DEVISE", mockRetraitDeclarCreate.getDevise());

                response = myDao.getDaoImpl().getStoredProcedureOutput("F_NEW_RETRAIT_DECLARATION", "CODE_DECLARATION", bulkRetrDecl);
                resultJson.put("declarationId", response);

            }

        } catch (Exception e) {
            resultJson.put("code", "300");
            resultJson.put("message", "Error");
        }
        return resultJson.toString();
    }

    public static boolean savePeriodeDeclaration(PeriodeDeclaration periodeDeclaration) {

        boolean result;

        try {

            String query = "EXEC F_NEW_PERIODE_DECLARATION ?1,?2,?3,?4,?5,?6,?7";

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    periodeDeclaration.getAssujetissement().getId(),
                    periodeDeclaration.getDebut(),
                    periodeDeclaration.getFin(),
                    periodeDeclaration.getDateLimite(),
                    null, null, null);

        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public static Taux getTauxByDevise(String devise) {
        String sqlQuery = "SELECT TOP 1 * FROM T_TAUX WITH (READPAST) WHERE DEVISE = ?1 AND DEVISE_DEST = 'CDF' ORDER BY ID DESC";
        List<Taux> tauxs = (List<Taux>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Taux.class), false, devise.trim());
        return tauxs.isEmpty() ? null : tauxs.get(0);
    }

    @Override
    public BonAPayer getBonPayerByCode(String code) {

        try {

            String query = "SELECT * FROM T_BON_A_PAYER WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";

            List<BonAPayer> baps = (List<BonAPayer>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BonAPayer.class), false, code);

            return baps.isEmpty() ? null : baps.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Journal> getTransactions(String agentId) {

        try {

            String query = "SELECT * FROM T_JOURNAL WITH (READPAST) WHERE AGENT_CREAT = ?1 AND ETAT = 2";

            List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Journal.class), false, agentId);

            return journals;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Tarif> getRates(String ab) {

        try {

            String query = "SELECT * FROM T_TARIF WITH (READPAST) WHERE CODE IN(SELECT TARIF FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1) ORDER BY INTITULE ASC";

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, ab);

            return tarifs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static PeriodeDeclaration getPeriodeDeclarationRequerant(String codeAss, int year, int month, String periodicite, String codeAB, boolean forYear) {

        try {

            String query;
            List<PeriodeDeclaration> periodeDeclarationList;

            if (!forYear) {
                query = query = "SELECT P.* FROM T_PERIODE_DECLARATION P WHERE P.ASSUJETISSEMENT = ?1 AND YEAR(P.DEBUT) = ?2 AND MONTH(P.DEBUT) = ?3 "
                        + "AND P.ASSUJETISSEMENT IN(SELECT A.ID FROM T_ASSUJETI A WHERE ARTICLE_BUDGETAIRE = (SELECT AB.CODE FROM T_ARTICLE_BUDGETAIRE AB "
                        + "WHERE AB.PERIODICITE = ?4 AND AB.CODE = ?5))";

                periodeDeclarationList = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAss, year, month, periodicite, codeAB);

            } else {
                query = query = "SELECT P.* FROM T_PERIODE_DECLARATION P WHERE P.ASSUJETISSEMENT = ?1 AND YEAR(P.DEBUT) = ?2 "
                        + "AND P.ASSUJETISSEMENT IN(SELECT A.ID FROM T_ASSUJETI A WHERE ARTICLE_BUDGETAIRE = (SELECT AB.CODE FROM T_ARTICLE_BUDGETAIRE AB "
                        + "WHERE AB.PERIODICITE = ?3 AND AB.CODE = ?4))";

                periodeDeclarationList = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAss, year, periodicite, codeAB);
            }

            return periodeDeclarationList.isEmpty() ? null : periodeDeclarationList.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Acquisition> getBiensPersonne(String codePersonne) {

        String SELECT_BIENS_OF_PERSONNE = "SELECT * FROM T_ACQUISITION WITH (READPAST) WHERE PERSONNE IN"
                + "(SELECT FK_PERSONNE FROM T_LOGIN_WEB WHERE FK_PERSONNE = ?1) AND ETAT = 1";

        try {
            String query = SELECT_BIENS_OF_PERSONNE;
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static LoginWeb getLoginWeb(String username) {

        try {

            String LOGIN_AGENT = "SELECT * FROM T_LOGIN_WEB WHERE USERNAME= ?1 AND ETAT = 1";

            List<LoginWeb> agents = (List<LoginWeb>) myDao.getDaoImpl().find(LOGIN_AGENT,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, username);

            return agents.isEmpty() ? null : agents.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static LoginWeb getLoginWeb(String username, String password) {

        try {

            String LOGIN_AGENT = "SELECT * FROM T_LOGIN_WEB WHERE USERNAME=?1 AND PASSWORD =  HASHBYTES ('MD5','" + password.trim() + "')";

            List<LoginWeb> agents = (List<LoginWeb>) myDao.getDaoImpl().find(LOGIN_AGENT,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, username);

            return agents.isEmpty() ? null : agents.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean setEtatAssujettiLoginWeb(String mail, String phone, String username, String password) {

        boolean result = false;

        try {

            String sqlQuery = "SELECT * FROM T_LOGIN_WEB WHERE USERNAME = ?1 AND PASSWORD =  HASHBYTES ('MD5','" + password.trim() + "') AND ETAT = 2";

            List<LoginWeb> agents = (List<LoginWeb>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, username);

            if (!agents.isEmpty()) {
                sqlQuery = "UPDATE T_LOGIN_WEB SET ETAT = 1, MAIL = ?1, TELEPHONE = ?2 WHERE USERNAME = ?3 AND PASSWORD =  HASHBYTES ('MD5','" + password.trim() + "')";
                result = myDao.getDaoImpl().executeNativeQuery(sqlQuery, mail, phone, username);
            }

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getListPersonne(String name, String postnom, String prenom, String dateNaissance, String complementDateNaissance, String modeSearch) {

        try {

            List<Personne> personnes;

            String SELECT_PERSONNE_BY_NAME = " SELECT * FROM T_PERSONNE P WITH (READPAST) "
                    + " INNER JOIN T_COMPLEMENT CP "
                    + " ON P.CODE = CP.PERSONNE "
                    + " WHERE (%s) "
                    + " AND P.ETAT IN (1) AND FORME_JURIDIQUE = '04' %s";

            String SELECT_PERSONNE_BY_NAME_MORALE = "SELECT * FROM T_PERSONNE P WITH (READPAST) WHERE (lower(NOM) LIKE ?1 OR lower(POSTNOM) LIKE ?2 OR lower(PRENOM) LIKE ?3) AND FORME_JURIDIQUE = '03' AND P.ETAT = 1";

            String AND_VALEUR_COMPLEMENT_WITH_DATE = "AND (CP.FK_COMPLEMENT_FORME = '%s' AND CAST(CP.VALEUR as varchar) = '%s')";

            String AND_VALEUR_COMPLEMENT_WITHOUT_DATE = "AND (CP.FK_COMPLEMENT_FORME = '%s')";

            if (modeSearch.equals(GeneralConst.NumberString.ONE)) {

                String QUERY_PART = "(lower(P.NOM) = '%s' OR lower(P.POSTNOM) = '%s' OR lower(P.PRENOMS) = '%s')";

                String QUERY_PART_OR = " OR ";

                String COMPOSITY_QUERY_PART = GeneralConst.EMPTY_STRING;

                if (!name.isEmpty()) {

                    String QUERY_PART_NAME = String.format(QUERY_PART, name.toLowerCase(), name.toLowerCase(), name.toLowerCase());
                    COMPOSITY_QUERY_PART += QUERY_PART_NAME;

                }

                if (!postnom.isEmpty()) {

                    COMPOSITY_QUERY_PART += !name.isEmpty() ? QUERY_PART_OR : GeneralConst.EMPTY_STRING;
                    String QUERY_PART_POSTNOM = String.format(QUERY_PART, postnom.toLowerCase(), postnom.toLowerCase(), postnom.toLowerCase());
                    COMPOSITY_QUERY_PART += QUERY_PART_POSTNOM;
                }

                if (!prenom.isEmpty()) {

                    if (!name.isEmpty() || !postnom.isEmpty()) {
                        COMPOSITY_QUERY_PART += QUERY_PART_OR;
                    }
                    String QUERY_PART_PRENOM = String.format(QUERY_PART, prenom.toLowerCase(), prenom.toLowerCase(), prenom.toLowerCase());
                    COMPOSITY_QUERY_PART += QUERY_PART_PRENOM;
                }

                String QUERY;

                if (!dateNaissance.isEmpty()) {
                    String queryValeurComplement = String.format(AND_VALEUR_COMPLEMENT_WITH_DATE, complementDateNaissance, dateNaissance);
                    QUERY = String.format(SELECT_PERSONNE_BY_NAME, COMPOSITY_QUERY_PART, queryValeurComplement);
                } else {
                    String queryValeurComplement = String.format(AND_VALEUR_COMPLEMENT_WITHOUT_DATE, complementDateNaissance);
                    QUERY = String.format(SELECT_PERSONNE_BY_NAME, COMPOSITY_QUERY_PART, queryValeurComplement);
                }

                personnes = (List<Personne>) myDao.getDaoImpl().find(QUERY,
                        Casting.getInstance().convertIntoClassType(Personne.class), false);

            } else {

                String QUERY = SELECT_PERSONNE_BY_NAME_MORALE;

                personnes = (List<Personne>) myDao.getDaoImpl().find(QUERY,
                        Casting.getInstance().convertIntoClassType(Personne.class), true, name.toLowerCase(), name.toLowerCase(), name.toLowerCase());
            }

            return personnes;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static ComplementBien getDetailsByLocationId(String valeurLocalisation) {

        try {

//            String query = "SELECT * FROM T_COMPLEMENT_BIEN WHERE TYPE_COMPLEMENT = '0000000102' AND VALEUR = ?1";
            String query = "SELECT * FROM T_COMPLEMENT_BIEN CB WITH (READPAST) WHERE CB.TYPE_COMPLEMENT = '0000000102' "
                    + "AND CB.VALEUR = ?1 AND CB.TYPE_COMPLEMENT IN (SELECT TCB.CODE FROM T_TYPE_COMPLEMENT_BIEN TCB "
                    + "WITH (READPAST) WHERE TCB.TYPE_BIEN = 'TB00000001')";

            List<ComplementBien> cbs = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, valeurLocalisation);

            return cbs.isEmpty() ? null : cbs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Personne getPersonById(String id) {
        try {

            String query = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE CODE = ?1";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, id);

            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static NoteCalcul getNC(String bien) {

        try {

            String query = "SELECT * FROM T_NOTE_CALCUL WITH (READPAST) WHERE BIEN = ?1";

            List<NoteCalcul> nc = (List<NoteCalcul>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NoteCalcul.class), false, bien);

            return nc.isEmpty() ? null : nc.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Tarif getTarif(ArticleBudgetaire ab) {

        try {

            String query = "SELECT * FROM T_TARIF WITH (READPAST) WHERE CODE IN(SELECT TARIF FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1) ORDER BY INTITULE ASC";

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, ab.getCode());

            return tarifs.isEmpty() ? null : tarifs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static NotePerception getNP(String nc) {

        try {

            String query = "SELECT * FROM T_NOTE_PERCEPTION WITH (READPAST) WHERE NOTE_CALCUL = ?1";

            List<NotePerception> np = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, nc);

            return np.isEmpty() ? null : np.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ComplementBien> getDetailsByLocation(String valeurLocalisation, String typeComplementBien, String typeBien) {

        try {

            String query = "SELECT * FROM T_COMPLEMENT_BIEN CB WITH (READPAST) WHERE CB.TYPE_COMPLEMENT = ?1 "
                    + "AND CB.VALEUR = ?2 AND CB.TYPE_COMPLEMENT IN (SELECT TCB.CODE FROM T_TYPE_COMPLEMENT_BIEN TCB "
                    + "WITH (READPAST) WHERE TCB.TYPE_BIEN = ?3)";

            List<ComplementBien> cbs = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, typeComplementBien, valeurLocalisation, typeBien);

            return cbs;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Personne> getPersonsById(String id) {
        try {

            String query = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE CODE = ?1";

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, id);

            return personnes;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<NotePerception> getNotePerception(String bien) {

        try {

            String query = "SELECT * FROM T_NOTE_PERCEPTION WITH (READPAST) WHERE NOTE_CALCUL IN("
                    + "SELECT NUMERO FROM T_NOTE_CALCUL WITH (READPAST) WHERE BIEN =?1)";

            List<NotePerception> nps = (List<NotePerception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerception.class), false, bien);

            return nps;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Bien> getBien(String typeComplementBien) {

        try {

            String query = "SELECT * FROM T_BIEN WITH (READPAST) where id in (select bien from T_COMPLEMENT_BIEN "
                    + "WITH (READPAST) WHERE TYPE_COMPLEMENT = ?1)";

            List<Bien> bien = (List<Bien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bien.class), false, typeComplementBien);

            return bien;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Bordereau getBordereauByNumero(String numero, boolean isDeclaration) {

        try {

            String query = !isDeclaration
                    ? "SELECT * FROM T_BORDEREAU WITH (READPAST) WHERE NUMERO_BORDEREAU = ?1"
                    : "SELECT * FROM T_BORDEREAU WITH (READPAST) WHERE NUMERO_DECLARATION = ?1";

            List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bordereau.class), false, numero);

            return bordereaus.isEmpty() ? null : bordereaus.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public BanqueAb getAbByCompteAb(String Ab, String compte) {

        try {

            String query = "SELECT * FROM T_BANQUE_AB WHERE FK_AB = ?1 AND FK_BANQUE in (SELECT BANQUE from T_COMPTE_BANCAIRE where CODE = ?2) and (FK_COMPTE = ?2 OR FK_COMPTE = '*') and ETAT = 1";

            List<BanqueAb> compteBancaires = (List<BanqueAb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(BanqueAb.class), false, Ab, compte);

            return compteBancaires.isEmpty() ? null : compteBancaires.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Palier> getTarifByArticle(String ab) {

        try {

            String query = "SELECT * FROM T_PALIER (READPAST) where ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1";

            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, ab);

            return paliers;

        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitByPeriode(Integer periode) {

        String SELECT_BIENS_OF_PERSONNE = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE FK_PERIODE = ?1 AND ETAT = 1";

        try {
            String query = SELECT_BIENS_OF_PERSONNE;
            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, periode);
            return !declarations.isEmpty() ? declarations.get(0) : null;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Assujeti getAssujettiByTaxeID(String taxeID, String bienID) {

        String SELECT_BIENS_OF_PERSONNE = "SELECT * FROM T_ASSUJETI WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND BIEN = ?2 AND ETAT = 1";

        try {
            String query = SELECT_BIENS_OF_PERSONNE;
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false, taxeID, bienID);
            return !assujetis.isEmpty() ? assujetis.get(0) : null;
        } catch (Exception e) {
            throw (e);
        }
    }

    @Override
    public List<Journal> getJournal(String reference) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Bordereau> getBordereau(String reference) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Service> getServiceAssiete() {

        try {

            String query = "SELECT * FROM T_SERVICE WITH (READPAST) WHERE ETAT = 1 AND SERVICE IS NOT NULL";

            List<Service> services = (List<Service>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Service.class), false);

            return services;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<ArticleBudgetaire> articleBudgetaires(String service) {

        try {

            String query = "SELECT * FROM T_ARTICLE_BUDGETAIRE WITH (READPAST) WHERE ETAT = 1 AND ARTICLE_GENERIQUE "
                    + " IN( SELECT b.CODE FROM T_ARTICLE_GENERIQUE b WITH (READPAST) WHERE b.SERVICE_ASSIETTE = ?1 AND b.ETAT = 1)";

            List<ArticleBudgetaire> abs = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, service);

            return abs;

        } catch (Exception e) {
            throw e;
        }
    }

    public static Personne getPersonne(String mail) {

        try {

            String query = "SELECT * FROM T_PERSONNE WHERE CODE IN("
                    + "SELECT FK_PERSONNE FROM T_LOGIN_WEB WHERE MAIL = ?1) AND ETAT = 1";

            List<Personne> ps = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, mail);

            return ps.isEmpty() ? null : ps.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean updatePassword(String idUser, String password) {

        String query = "UPDATE T_LOGIN_WEB SET PASSWORD = HASHBYTES ('MD5','" + password.trim()
                + "'), ETAT = 1 WHERE FK_PERSONNE = ?1";

        try {
            return myDao.getDaoImpl().executeNativeQuery(query, idUser);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean setSendMailTrue(int etat, String idUser) {

        String query = "UPDATE T_LOGIN_WEB SET SEND_MAIL = ?1 WHERE FK_PERSONNE = ?2";

        try {
            return myDao.getDaoImpl().executeNativeQuery(query, etat, idUser);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkPassword(String lastPassword, String lastPasswordBdd) {

        HashMap<String, Object> params = new HashMap<>();
        boolean rep = false;

        try {
            String queryStr = "SELECT [dbo].CRIPTE_PASSWORD('" + lastPassword.trim() + "')";

            String result = myDao.getDaoImpl().getSingleResultToString(queryStr);

            if (!result.isEmpty()) {
//                returnValue = result;
                if (result.equals(lastPasswordBdd.trim())) {
                    rep = true;
                }
            }
        } catch (Exception e) {
            throw (e);
        }
        return rep;
    }

    public static String saveCustumer2(Personne personne, String adresse) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counterP = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsP = new HashMap<>();

        String firstStoredProcedure = "F_CREATE_PERSONNE";
        String firstStoredReturnValueKey = "PERSONNE";

        paramsP.put("NOM", personne.getNom().trim());
        paramsP.put("POSTNOM", personne.getPostnom().trim());
        paramsP.put("PRENOM", personne.getPrenoms().trim());
        paramsP.put("FORME", personne.getFormeJuridique().getCode().trim());
        paramsP.put("AGENT_CREATE", personne.getAgentCreat());
        paramsP.put("NIF", personne.getNif().trim());

        String F_CREATE_LOGIN_WEB = ":EXEC F_CREATE_LOGIN_WEB '%s',?1,?2,?3,?4,?5";

        String F_COMPLEMENT = ":EXEC F_NEW_PERSONNE_COMPLEMENT ?1,'%s',?2,?3";

        String ADD_ADRESSE_WEB = ":EXEC ADD_ADRESSE_WEB_V2 ?1,'%s',?2,?3";

        counterP++;
        firstBulk.put(counterP + ADD_ADRESSE_WEB, new Object[]{
            adresse, 1, 0
        });

        counterP++;
        firstBulk.put(counterP + F_CREATE_LOGIN_WEB, new Object[]{
            personne.getLoginWeb().getMail(),
            personne.getLoginWeb().getPassword(),
            personne.getLoginWeb().getTelephone(),
            personne.getLoginWeb().getMail(),
            personne.getAgentCreat()});

        for (Complement cp : personne.getComplementList()) {

            counterP++;

            firstBulk.put(counterP + F_COMPLEMENT, new Object[]{
                cp.getFkComplementForme().getCode().trim(),
                cp.getValeur(),
                personne.getAgentCreat()
            });
        }

        String result;

        try {

            result = myDao.getDaoImpl().execBulkQueryv4(
                    firstStoredProcedure, firstStoredReturnValueKey, paramsP, firstBulk);

        } catch (Exception e) {
            throw e;
        }

        return result;

    }

    public static BanqueAb getBanqueAbByArticleB(ArticleBudgetaire ab) {

        try {

            String request = "SELECT * FROM T_BANQUE_AB WHERE FK_AB = ?1";

            List<BanqueAb> banqueAbs = (List<BanqueAb>) myDao.getDaoImpl().find(request,
                    Casting.getInstance().convertIntoClassType(BanqueAb.class), false, ab.getCode());

            return banqueAbs.isEmpty() ? null : banqueAbs.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static String saveNC(NoteCalcul newNoteCalcul) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counterNC = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsNC = new HashMap<>();

        String firstStoredProcedure = "F_NEW_NOTE_CALCUL4";
        String firstStoredReturnValueKey = "NC_RETURN";

        paramsNC.put("PERSONNE", newNoteCalcul.getPersonne());
        paramsNC.put("ADRESSE", newNoteCalcul.getFkAdressePersonne());
        paramsNC.put("EXERCICE", newNoteCalcul.getExercice());
        paramsNC.put("SITE", newNoteCalcul.getSite());
        paramsNC.put("SERVICE", newNoteCalcul.getService());
        paramsNC.put("AGENT_CREAT", newNoteCalcul.getAgentCreat());
        paramsNC.put("DATE_CREAT", newNoteCalcul.getDateCreat());
        paramsNC.put("DEPOT_DECLARATION", null);
        paramsNC.put("BIEN", null);
        paramsNC.put("CPI_FICHE", null);

        String EXEC_F_NEW_DNC = ":EXEC F_NEW_DNC ?1,'%s',?2,?3,?4,?5,?6,?7,?8,?9,?10";

//        String EXEC_F_UPDATE = ":UPDATE T_NOTE_CALCUL SET AGENT_CLOTURE = ?1, DATE_CLOTURE = ?2, ETAT = 1 WHERE NUMERO = '%s'";
        for (DetailsNc abt : newNoteCalcul.getDetailsNcList()) {

            counterNC++;

            firstBulk.put(counterNC + EXEC_F_NEW_DNC, new Object[]{
                abt.getArticleBudgetaire().getCode().trim(),
                abt.getTauxArticleBudgetaire(),
                abt.getValeurBase(),
                abt.getMontantDu(),
                abt.getQte(),
                0,
                abt.getTypeTaux().trim(),
                0,
                abt.getTarif(),
                abt.getDevise().getCode()

            });

        }

        counterNC++;

//        firstBulk.put(counterNC , new Object[]{
//            newNoteCalcul.getAgentCreat(),
//            newNoteCalcul.getDateCreat()
//        });
        String result;

        try {

            result = myDao.getDaoImpl().execBulkQueryv4(
                    firstStoredProcedure, firstStoredReturnValueKey, paramsNC, firstBulk);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static String saveNewAdress(String nif, String chaine) {

        String result;

        try {

            HashMap<String, Object> bulkBien = new HashMap<>();
            bulkBien.put("CHAINE", chaine);
            bulkBien.put("PERSONNE", nif);
            bulkBien.put("PARDEFAUT", 0);

            result = myDao.getDaoImpl().getStoredProcedureOutput("ADD_ADRESSE_WEB_V2", "ID_A_P", bulkBien);

        } catch (Exception e) {
            throw e;
        }
        return result;

    }

    public static LoginWeb getLoginWebByTelephoneOrEmail(String email, String telephone) {

        try {

            String SELECT_LOGINWEB_BY_TELEPHONE_EMAIL = "SELECT * FROM T_LOGIN_WEB WITH (READPAST) "
                    + " WHERE MAIL = ?1";

            List<LoginWeb> loginWeb = (List<LoginWeb>) myDao.getDaoImpl().find(SELECT_LOGINWEB_BY_TELEPHONE_EMAIL,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, email);

            return loginWeb.isEmpty() ? null : loginWeb.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<AdressePersonne> getAdressePersonneById(String id) {
        try {

            String query = "SELECT * FROM T_ADRESSE_PERSONNE WITH (READPAST) WHERE CODE = ?1";

            List<AdressePersonne> adressePersonnes = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, id);

            return adressePersonnes;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<NoteCalcul> getTaxation(String nif) {
        try {

            String query = "SELECT * FROM T_NOTE_CALCUL WITH (READPAST) WHERE PERSONNE = ?1";

            List<NoteCalcul> ncs = (List<NoteCalcul>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NoteCalcul.class), false, nif);
            return ncs;

        } catch (Exception e) {
            throw e;
        }
    }

    public static LoginWeb getLoginWeb_V2(String nif) {

        try {

            String LOGIN_AGENT = "SELECT * FROM T_LOGIN_WEB WHERE FK_PERSONNE = ?1 AND ETAT = 1";

            List<LoginWeb> agents = (List<LoginWeb>) myDao.getDaoImpl().find(LOGIN_AGENT,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, nif);

            return agents.isEmpty() ? null : agents.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static LoginWeb checkLoginWeb(String mail, String password) {

        boolean response = false;
        try {
//            String part1 = "HASHBYTES('MD5','" + password + "')";
            String query = "SELECT * FROM T_LOGIN_WEB WHERE MAIL = ?1 AND PASSWORD = HASHBYTES('MD5','" + password + "')";

            List<LoginWeb> lws = (List<LoginWeb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, mail);

            return lws.isEmpty() ? null : lws.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailsNc> getDetailsNC(String numero) {
        try {

            String query = "SELECT * FROM T_DETAILS_NC WITH (READPAST) WHERE NOTE_CALCUL = ?1 AND ETAT = 1";

            List<DetailsNc> dns = (List<DetailsNc>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsNc.class), false, numero);
            return dns;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<EntiteAdministrative> getEntiteAdmin(String codeProvinceHk) {
        try {

            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE WHERE ENTITE_MERE = ?1 AND TYPE_ENTITE = 4 AND ETAT = 1";

            List<EntiteAdministrative> easList = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, codeProvinceHk);
            return easList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static Palier getPalierForBienImmobilier(
            String codeArticle,
            String codeTarif,
            String codeFormeJuridique,
            String codeCommune,
            float valeurBase,
            boolean isPalier,
            String codeTypeBien) {

        List<Palier> listPaliers = new ArrayList<>();

        if (!isPalier) {

            String sqlQuery = GeneralConst.EMPTY_STRING;

            switch (codeArticle) {

                case "00000000000002282020": // RL
                case "00000000000002292020": // IRL

                    sqlQuery = "SELECT top 1 * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1";

                    listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery, Casting.getInstance().convertIntoClassType(Palier.class), false,
                            codeArticle.trim());
                    break;
                case "00000000000002272020": // IF

                    sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 "
                            + " AND TYPE_PERSONNE = ?3 AND FK_ENTITE_ADMINISTRATIVE = ?4 AND ETAT = 1 AND FK_TYPE_BIEN = ?5";

                    listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery, Casting.getInstance().convertIntoClassType(Palier.class), false,
                            codeArticle.trim(), codeTarif.trim(), codeFormeJuridique.trim(), codeCommune.trim(), codeTypeBien.trim());
                    break;

                case "00000000000002302020": // VIGNETTE

                    sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3";

                    listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                            Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJuridique, codeTarif);

                    break;
                case "00000000000002312020": //ICM

                    break;
                default:
                    sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 "
                            + " AND TYPE_PERSONNE = ?3 AND FK_ENTITE_ADMINISTRATIVE = ?4 AND ETAT = 1 AND FK_TYPE_BIEN = ?5";

                    listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery, Casting.getInstance().convertIntoClassType(Palier.class), false,
                            codeArticle.trim(), codeTarif.trim(), codeFormeJuridique.trim(), codeCommune.trim(), codeTypeBien.trim());
                    break;
            }

        } else {
            String sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST)"
                    + " WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND (TYPE_PERSONNE = ?3)"
                    + " AND ETAT = 1 AND (TAUX >= BORNE_INFERIEUR AND TAUX <= BORNE_SUPERIEUR) AND FK_ENTITE_ADMINISTRATIVE = ?4";

            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false,
                    codeArticle.trim(), codeTarif.trim(), codeFormeJuridique.trim(), valeurBase, codeCommune.trim());
        }

        return listPaliers.isEmpty() ? null : listPaliers.get(0);
    }

    public static List<Assujeti> getAssujettissementByPersonne(String codePersonne) {
        try {
            String query = "SELECT A.* FROM T_ASSUJETI A WITH (READPAST) WHERE A.PERSONNE = ?1 AND A.ETAT = 1";
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class),
                    false,
                    codePersonne);
            return assujetis;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Unite getUnite(String code) {

        try {

            String query = "SELECT * FROM T_unite WHERE CODE = ?1 AND  ETAT = 1";

            List<Unite> unites = (List<Unite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Unite.class), false, code);

            return unites.isEmpty() ? null : unites.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static TypeComplement getTypeComplementbyComplementBien(String codeComplementBien) {

        try {

            String query = "SELECT * from T_TYPE_COMPLEMENT WHERE CODE IN (SELECT COMPLEMENT FROM T_TYPE_COMPLEMENT_BIEN WHERE CODE IN (select TYPE_COMPLEMENT from T_COMPLEMENT_BIEN WHERE ID = ?1))";

            List<TypeComplement> typeComplements = (List<TypeComplement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeComplement.class), false, codeComplementBien);

            return typeComplements.isEmpty() ? null : typeComplements.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<PeriodeDeclaration> getListPeriodeDeclarationByAssuj(String code) {
        try {
            String query = "SELECT * FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ASSUJETISSEMENT = ?1 "
                    + " AND PD.ETAT = 1 AND PD.ID NOT IN (SELECT ISNULL(FK_PERIODE,0) FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT > 0) order by ID";
            return (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, code);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<PeriodeDeclaration> getListPeriodeDeclarationByAssujV2(String code) {
        try {
            String query = "SELECT * FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ASSUJETISSEMENT = ?1 "
                    + " AND PD.ETAT = 1 AND PD.ID IN (SELECT ISNULL(FK_PERIODE,0) FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT > 0) order by ID";
            return (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, code);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static IcmParam getIcmParamByTarifAndAnnee(String tarifCode, int annee) {

        try {

            String query = "SELECT * FROM T_ICM_PARAM WITH (READPAST) WHERE TARIF = ?1 AND ANNEE = ?2 AND ETAT = 1";

            List<IcmParam> icmParams = (List<IcmParam>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(IcmParam.class), false, tarifCode, annee);

            return icmParams.isEmpty() ? null : icmParams.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Integer getDateDiffBetwenTwoDates(String echeance) {
        String queryStr = "SELECT dbo.F_DATEDIFF_BY_MONTH_BETWEN_TWO_DATES('" + echeance.trim() + "')";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

    public static Integer checkRecidivisteDeclaration(String assujettisement, Integer periode) {

        Integer result = 0;

        String masterQuery = String.format("SELECT dbo.F_GET_RECIDIVISTE_DECLARATION('%s',%s)", assujettisement, periode);

        try {
            result = myDao.getDaoImpl().getSingleResultToInteger(masterQuery);
        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Adresse getAdresseDefaultByAssujetti(String codeAssujetti) {
        String sqlQuery = "SELECT * FROM T_ADRESSE WITH (READPAST) "
                + " WHERE ID IN (SELECT DISTINCT ADRESSE FROM T_ADRESSE_PERSONNE WITH (READPAST) "
                + " WHERE PERSONNE = ?1 AND PAR_DEFAUT = 1 AND ETAT = 1)";
        List<Adresse> listAdresses = (List<Adresse>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Adresse.class),
                false, codeAssujetti.trim());
        Adresse adresse = !listAdresses.isEmpty() ? listAdresses.get(0) : null;
        return adresse;
    }

    public static List<Assujeti> getAssujettissement(String codepersonne, String codeAb) {
        String sqlQuery = "SELECT * FROM T_ASSUJETI WITH (READPAST) WHERE PERSONNE = ?1 AND ARTICLE_BUDGETAIRE = ?2 AND ETAT = 1";
        List<Assujeti> listAssuj = (List<Assujeti>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Assujeti.class),
                false, codepersonne, codeAb);
        return listAssuj;
    }

    public static Tarif getTarifByCode(String code) {
        String sqlQuery = "SELECT * FROM T_TARIF WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";
        List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Tarif.class), false, code.trim());
        return tarifs.isEmpty() ? null : tarifs.get(0);
    }

    public static Unite getUnitebyCode(String code) {
        String sqlQuery = "SELECT * FROM T_UNITE WITH (READPAST) WHERE CODE = ?1";
        List<Unite> tarifs = (List<Unite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Unite.class), false, code.trim());
        return tarifs.isEmpty() ? null : tarifs.get(0);
    }

    public static String getNewid() {

        HashMap<String, Object> params = new HashMap<>();
        String query = "F_GET_NEWID";
        String returnValue = "ID";

        params.put("X", "");

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);
        return result;
    }

    public static Boolean saveRetraitDeclarationV2(List<DeclarationMock> declarationListMock, String numDeclaration, DeclarationPenaliteMock declarationPenaliteMock) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (DeclarationMock declarationMock : declarationListMock) {

                //counter++;
                for (PeriodeDeclarationSelection pds : declarationMock.getListPeriode()) {

                    counter++;

                    bulkQuery.put(counter + ":EXEC F_NEW_RETRAIT_DECLARATION_TDT ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16,?17", new Object[]{
                        declarationMock.getRequerant(),
                        declarationMock.getAssujetti(),
                        declarationMock.getCodeArticleBudgetaire(),
                        pds.getPeriode(),
                        declarationMock.getCodeAgentCreate(),
                        declarationMock.getCodeSite(),
                        null,
                        pds.getPrincipal(),
                        declarationMock.getDevise(),
                        numDeclaration,//declarationMock.getCodeDeclaration(),
                        pds.getPenalise(),
                        pds.getPenalite(),
                        declarationMock.getBanque(),
                        declarationMock.getCompteBancaire(),
                        declarationMock.getAmountRemisePenalite().floatValue(),
                        declarationMock.getTauxRemise(),
                        declarationMock.getObservationRemise()
                    });

                    if (declarationMock.getCodeArticleBudgetaire().equals("00000000000002312020")) {

                        counter++;

                        bulkQuery.put(counter + ":UPDATE T_PERIODE_DECLARATION SET OBSERVATION = ?1 WHERE ID = ?2", new Object[]{
                            declarationMock.getIcmParamId(),
                            Integer.valueOf(pds.getPeriode())
                        });
                    }

                }
            }

            if (declarationPenaliteMock != null) {

                counter++;

                bulkQuery.put(counter + ":EXEC F_NEW_RETRAIT_DECLARATION_PENALITE ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14,?15,?16", new Object[]{
                    declarationListMock.get(0).getRequerant(),
                    declarationListMock.get(0).getAssujetti(),
                    declarationListMock.get(0).getCodeArticleBudgetaire(),
                    null,
                    declarationListMock.get(0).getCodeAgentCreate(),
                    null,
                    declarationPenaliteMock.getAmountPenalite(),
                    declarationListMock.get(0).getDevise(),
                    numDeclaration,//declarationMock.getCodeDeclaration(),
                    0,
                    0,
                    declarationListMock.get(0).getBanque(),
                    declarationListMock.get(0).getCompteBancaire(),
                    0,//declarationMock.getAmountRemisePenalite().floatValue(),
                    declarationPenaliteMock.getTauxRemise(),
                    declarationPenaliteMock.getObservationRemise()
                });

            }

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }

        return result;

    }

    public static List<Division> getDivisionList(String code_division) {
        String sqlQuery = "SELECT * FROM T_DIVISION WITH (READPAST) WHERE ETAT = 1 AND CODE %s";
        sqlQuery = String.format(sqlQuery, code_division);
        List<Division> ds = (List<Division>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Division.class), false);
        return ds;
    }

    public static List<Site> getSiteByDivision(String division_code) {
        String sqlQuery = "SELECT * FROM T_SITE WITH (READPAST) WHERE ETAT = 1 AND PEAGE = 0 AND FK_DIVISION = ?1";
        List<Site> sites = (List<Site>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Site.class), false, division_code);
        return sites;
    }

    public static Archive getArchiveByRefDocument(String reference) {
        try {
            String sqlQuery = "SELECT * FROM T_ARCHIVE WITH(READPAST) WHERE REF_DOCUMENT = ?1 AND DOCUMENT_STRING IS NOT NULL";
            List<Archive> archives = (List<Archive>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Archive.class), false, reference.trim());
            return archives.isEmpty() ? null : archives.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent getAgentByCode(String codeAgent) {
        try {
            String query = "SELECT * FROM T_AGENT  WITH (READPAST) WHERE CODE  = ?1";
            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeAgent);
            return agent.isEmpty() ? null : agent.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static Site getSiteByCode(String codeSite) {
        String sqlQuery = "SELECT * FROM T_SITE WHERE CODE = ?1";
        List<Site> sites = (List<Site>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Site.class), false, codeSite.trim());
        return sites.isEmpty() ? null : sites.get(0);
    }

    public static boolean updateNumeroDoc(
            String query) {

        boolean result;

        result = myDao.getDaoImpl().executeStoredProcedure(query);
        return result;
    }

    public static int getLastIncrement(String req) {
        String request = req;
        return myDao.getDaoImpl().getSingleResultToInteger(request);
    }

    public static Personne getPersonneByCode_V2(String codePersonne) {
        try {
            String query = "SELECT * FROM T_PERSONNE WITH(READPAST) WHERE CODE = ?1 AND ETAT = 1";
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, codePersonne);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static PeriodeDeclaration getPeriodeDeclarationByCode(String codePeriode) {
        try {
            String query = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ID = ?1";
            List<PeriodeDeclaration> periodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codePeriode);
            return periodeDeclarations.isEmpty() ? null : periodeDeclarations.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Banque getBanqueByCode(String codeBanque) {
        try {
            String query = "SELECT * FROM T_BANQUE WHERE CODE  = ?1";
            List<Banque> banques = (List<Banque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Banque.class), false, codeBanque);
            return banques.isEmpty() ? null : banques.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitDeclarationByPeriode(String periode) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE FK_PERIODE = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, periode);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static RetraitDeclaration getRetraiteclarationByID(int id) {
        String query = null;
        try {

            query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1";

            List<RetraitDeclaration> retrDcl = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, id);
            return retrDcl.size() > 0 ? retrDcl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationByNewID(String newId) {
        try {

            String sqlQuery;

            sqlQuery = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE NEW_ID = ?1 "
                    + "AND RETRAIT_DECLARATION_MERE IS NULL order by fk_Periode";
            List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class),
                    false, newId.trim());

            return retraitDeclarations;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static Integer getDateDiffBetwenTwoDates_V2(String debut, String fin) {
        String queryStr = "SELECT dbo.F_DATEDIFF_BY_MONTH_BETWEN_TWO_DATES_V2('" + debut.trim() + "','" + fin.trim() + "')";
        Integer result = myDao.getDaoImpl().getSingleResultToInteger(queryStr);
        return result;
    }

    public static List<Assujeti> getAssujettisByPeriodeDeclaration(String listPeriode) {
        String sqlQuery = "SELECT * FROM T_ASSUJETI WHERE ID IN(SELECT DISTINCT(ASSUJETISSEMENT) "
                + "FROM T_PERIODE_DECLARATION WHERE ID %s)";
        sqlQuery = String.format(sqlQuery, listPeriode);
        List<Assujeti> as = (List<Assujeti>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Assujeti.class), false);
        return as;
    }

    public static List<EntiteAdministrative> getEntiteAdministratives(String intitule) {
        try {
            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE "
                    + " WITH (READPAST) WHERE TYPE_ENTITE = 1 AND INTITULE LIKE ?1 AND ETAT = 1";
            List<EntiteAdministrative> entiteAdministratives = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), true, intitule);
            return entiteAdministratives;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String saveCustumer(Personne personne, List<AdressePersonne> adressePersonnes) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counterP = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsP = new HashMap<>();

        String firstStoredProcedure = "F_CREATE_PERSONNE";
        String firstStoredReturnValueKey = "PERSONNE";
//
//        paramsP.put("NOM", personne.getNom().trim());
//        paramsP.put("POSTNOM", personne.getPostnom().trim());
//        paramsP.put("PRENOM", personne.getPrenoms().trim());
//        paramsP.put("FORME", personne.getFormeJuridique().getCode().trim());
//        paramsP.put("AGENT_CREATE", personne.getAgentCreat());
//        paramsP.put("NIF", personne.getNif().trim());

        String F_CREATE_LOGIN_WEB = ":EXEC F_CREATE_LOGIN_WEB ?1,?2,?3,?4,?5";
        String F_COMPLEMENT = ":EXEC F_NEW_PERSONNE_COMPLEMENT ?1,'%s',?2,?3";
        String ADD_ADRESSE_WEB = ":EXEC ADD_ADRESSE_WEB ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14";

        counterP++;

        String codePersonne = createPersonne(personne);

        for (AdressePersonne adressePersonne : adressePersonnes) {
            firstBulk.put(counterP + ADD_ADRESSE_WEB, new Object[]{
                adressePersonne.getAdresse().getCommune(),
                adressePersonne.getAdresse().getCommune(),
                adressePersonne.getAdresse().getDistrict(),
                adressePersonne.getAdresse().getCommune(),
                adressePersonne.getAdresse().getQuartier(),
                adressePersonne.getAdresse().getAvenue(),
                adressePersonne.getAdresse().getNumero(),
                personne.getAgentCreat(),
                personne.getNif().trim(),
                adressePersonne.getParDefaut(),
                0,
                adressePersonne.getCode(),
                true,
                adressePersonne.getAdresse().getChaine()
            });
            counterP++;
        }

        counterP++;
        firstBulk.put(counterP + F_CREATE_LOGIN_WEB, new Object[]{
            codePersonne,
            personne.getLoginWeb().getMail(),
            personne.getLoginWeb().getPassword(),
            personne.getLoginWeb().getTelephone(),
            personne.getLoginWeb().getMail(),
            personne.getAgentCreat()});

        for (Complement cp : personne.getComplementList()) {

            counterP++;

            firstBulk.put(counterP + F_COMPLEMENT, new Object[]{
                cp.getFkComplementForme().getCode().trim(),
                cp.getValeur(),
                personne.getAgentCreat()
            });
        }

        String result;

        try {

            result = myDao.getDaoImpl().execBulkQueryv4(
                    firstStoredProcedure, firstStoredReturnValueKey, paramsP, firstBulk);

        } catch (Exception e) {
            throw e;
        }

        return result;

    }

    public static String createPersonne(Personne personne) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "PERSONNE";
        try {
            String query = "F_CREATE_PERSONNE";

            params.put("NOM", personne.getNom().trim());
            params.put("POSTNOM", personne.getPostnom().trim());
            params.put("PRENOM", personne.getPrenoms().trim());
            params.put("FORME", personne.getFormeJuridique().getCode().trim());
            params.put("AGENT_CREATE", personne.getAgentCreat());
            params.put("NIF", personne.getNif().trim());

            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static Boolean savePersonne(
            Personne personne,
            List<Complement> complements,
            List<AdressePersonne> adressePersonnes,
            LoginWeb loginWeb) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            if (personne.getCode() == null || personne.getCode().isEmpty()) {
                // NOUVEL ASSUJETTI
                String codePersonne = createPersonne(personne);
                if (codePersonne != null && !codePersonne.isEmpty()) {
                    for (Complement complement : complements) {
                        bulkQuery.put(counter + ":EXEC F_NEW_PERSONNE_COMPLEMENT ?1,?2,?3,?4", new Object[]{
                            complement.getFkComplementForme().getCode().trim(),
                            codePersonne,
                            complement.getValeur(),
                            personne.getAgentCreat()
                        });
                        counter++;
                    }
                    //INSERT INTO ADRESSE AND ADRESSE_PERSONNE
                    for (AdressePersonne adressePersonne : adressePersonnes) {
                        bulkQuery.put(counter + ":EXEC ADD_ADRESSE_WEB ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14", new Object[]{
                            adressePersonne.getAdresse().getProvince(),
                            adressePersonne.getAdresse().getCommune(),
                            adressePersonne.getAdresse().getDistrict(),
                            adressePersonne.getAdresse().getCommune(),
                            adressePersonne.getAdresse().getQuartier(),
                            adressePersonne.getAdresse().getAvenue(),
                            adressePersonne.getAdresse().getNumero(),
                            personne.getAgentCreat(),
                            codePersonne,
                            adressePersonne.getParDefaut(),
                            0,
                            adressePersonne.getCode(),
                            true,
                            adressePersonne.getAdresse().getChaine()
                        });
                        counter++;
                    }
                    //INSERT INTO LOGIN_WEB
                    if (loginWeb != null) {
                        bulkQuery.put(counter + ":EXEC F_CREATE_LOGIN_WEB ?1,?2,?3,?4,?5,?6", new Object[]{
                            codePersonne,
                            loginWeb.getMail(),
                            loginWeb.getPassword(),
                            loginWeb.getTelephone(),
                            loginWeb.getMail(),
                            personne.getAgentCreat()});
                        counter++;
                    }

                    result = executeQueryBulkInsert(bulkQuery);
                }
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<TypeBien> getTypeBienByType(int type) {
        try {
            String query = "SELECT TB.* FROM T_TYPE_BIEN TB WITH(READPAST) WHERE ETAT = 1"
                    + " AND TB.CODE IN (SELECT TBS.TYPE_BIEN FROM T_TYPE_BIEN_SERVICE TBS WITH(READPAST) "
                    + " WHERE TBS.ETAT = 1 AND TBS.TYPE = ?1) ORDER BY TB.INTITULE";
            List<TypeBien> tbs = (List<TypeBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBien.class), false, type);
            return tbs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListTarifVignetteOrConcessionMine(String codeTarif) {
        try {
            String query = "SELECT * FROM T_TARIF WITH (READPAST) WHERE ETAT = 1 AND CODE %s AND EST_TARIF_PEAGE = 0 ORDER BY INTITULE";

            query = String.format(query, codeTarif);

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListTarifPublicite(String codeAB) {
        try {

            String query = "SELECT t.CODE,t.INTITULE FROM T_TARIF t WITH (READPAST) WHERE t.ETAT = 1 \n"
                    + "AND t.CODE IN (select p.TARIF from T_PALIER p WITH (READPAST) \n"
                    + "	where ARTICLE_BUDGETAIRE %s and p.ETAT = 1) order by t.INTITULE";

            query = String.format(query, codeAB);

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<UsageBien> getListUsageBiens() {
        try {
            String query = "SELECT * FROM T_USAGE_BIEN WITH (READPAST) WHERE ETAT = 1 ORDER BY INTITULE";
            List<UsageBien> usageBiens = (List<UsageBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(UsageBien.class), false);
            return usageBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<EntiteAdministrative> getListCommuneHautKatanga(String districtList) {
        try {
            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE TYPE_ENTITE = 3 AND ETAT = 1"
                    + " AND ENTITE_MERE %s ORDER by INTITULE";

            query = String.format(query, districtList);
            List<EntiteAdministrative> communes = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false);
            return communes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<EntiteAdministrative> getListQuartierByCommune(String commune) {
        try {
            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE ENTITE_MERE = ?1 AND TYPE_ENTITE = 2 AND ETAT = 1 ORDER BY INTITULE";

            List<EntiteAdministrative> quartiers = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, commune);
            return quartiers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListCategorieImmobilierByQuartier(String codeQuartier) {
        try {
            String query = "SELECT t.* FROM T_TARIF t WITH (READPAST) WHERE t.CODE IN (SELECT p.TARIF FROM T_PALIER p WITH (READPAST)"
                    + " WHERE p.FK_ENTITE_ADMINISTRATIVE = ?1 AND p.ETAT = 1) ORDER BY t.INTITULE";

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, codeQuartier);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<LoginWeb> getLoginWebByNom(String nom) {
        try {
            String query = "SELECT * FROM T_LOGIN_WEB WHERE FK_PERSONNE IN(SELECT p.CODE FROM T_PERSONNE p "
                    + "WHERE p.NOM LIKE ?1 OR p.POSTNOM LIKE ?1 OR p.PRENOMS LIKE ?1)";

            List<LoginWeb> lws = (List<LoginWeb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), true, nom);
            return lws;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean setDemandeInscription(String mail, String phone, String nif) {
        try {
            String sqlQuery = "UPDATE T_LOGIN_WEB SET MAIL = ?1, ETAT = 3, TELEPHONE = ?2 WHERE FK_PERSONNE = ?3";
            return myDao.getDaoImpl().executeNativeQuery(sqlQuery, mail, phone, nif);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ComplementBien> getListComplementBien(String idBien) {
        try {
            String query = "SELECT * FROM T_COMPLEMENT_BIEN WITH (READPAST) WHERE BIEN =?1 AND ETAT= 1";
            List<ComplementBien> listComplementBien = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, idBien);
            return listComplementBien;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TypeComplementBien> getTypeComplementBienByTypeBien(String codeTypeBien) {
        try {
            String query = "SELECT * FROM T_TYPE_COMPLEMENT_BIEN WITH(READPAST) "
                    + " WHERE TYPE_BIEN = ?1 AND ETAT = 1";
            List<TypeComplementBien> typeComplementBiens = (List<TypeComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeComplementBien.class), false, codeTypeBien);
            return typeComplementBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Devise> getListAllDevises() {
        String sqlQuery = "SELECT * FROM T_DEVISE WITH (READPAST) WHERE ETAT = 1";
        List<Devise> listDevises = (List<Devise>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Devise.class), false);
        return listDevises;
    }

    public static List<Unite> getListAllUnites() {
        String sqlQuery = "SELECT * FROM T_UNITE WITH (READPAST) WHERE ETAT = 1 ORDER BY INTITULE ASC";
        List<Unite> listUnites = (List<Unite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Unite.class), false);
        return listUnites;
    }

    public static List<AdressePersonne> getAdressePersonneByPersonne(String codePersonne) {
        try {

            String query = "SELECT * FROM T_ADRESSE_PERSONNE WITH (READPAST) WHERE PERSONNE = ?1";

            List<AdressePersonne> adressePersonnes = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, codePersonne);

            return adressePersonnes;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Personne> getListAssujettisByCriterion(String valueSearch) {

        String sqlQuery = "SELECT a.CODE,a.NIF,a.NOM,"
                + " a.POSTNOM,a.PRENOMS,a.FORME_JURIDIQUE FROM T_PERSONNE a WITH (READPAST)"
                + " WHERE a.ETAT = 1 AND (a.CODE = ?1 OR a.NIF = ?1 OR a.NOM LIKE ?1 OR a.POSTNOM LIKE ?1 OR a.PRENOMS LIKE ?1) ORDER BY a.NOM,a.POSTNOM,a.PRENOMS";

        List<Personne> listAssujettis = (List<Personne>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Personne.class), true, valueSearch.trim());
        return listAssujettis;
    }

    public static Boolean updateBien(Bien bien, Acquisition acquisition, List<ComplementBien> complementBiens, Integer agentID, String type) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            if (bien.getId() != null || !bien.getId().isEmpty()) {

                if (type.equals("2")) {
                    bulkQuery.put(counter + ":UPDATE T_BIEN SET INTITULE = ?1, DESCRIPTION = ?2, TYPE_BIEN =?3, FK_ADRESSE_PERSONNE = ?4, FK_USAGE_BIEN = ?5,FK_TARIF = ?6,FK_COMMUNE = ?7, FK_QUARTIER = ?8 WHERE ID = ?9", new Object[]{
                        bien.getIntitule().trim(),
                        null,
                        bien.getTypeBien().getCode().trim(),
                        bien.getFkAdressePersonne() == null ? null : bien.getFkAdressePersonne().getCode().trim(),
                        bien.getFkUsageBien(),
                        bien.getFkTarif(),
                        bien.getFkCommune(),
                        bien.getFkQuartier(),
                        bien.getId().trim()
                    });
                } else {
                    bulkQuery.put(counter + ":UPDATE T_BIEN SET INTITULE = ?1, DESCRIPTION = ?2, TYPE_BIEN =?3, FK_ADRESSE_PERSONNE = ?4, FK_TARIF = ?5 WHERE ID = ?6", new Object[]{
                        bien.getIntitule().trim(),
                        bien.getDescription().trim(),
                        bien.getTypeBien().getCode().trim(),
                        bien.getFkAdressePersonne() == null ? null : bien.getFkAdressePersonne().getCode().trim(),
                        bien.getFkTarif(),
                        bien.getId().trim()
                    });
                }
                counter++;

                for (ComplementBien complementBien : complementBiens) {

                    if (complementBien.getId().equals(GeneralConst.EMPTY_STRING)) {
                        bulkQuery.put(counter + ":EXEC F_NEW_COMPLEMENT_BIEN ?1,?2,?3,?4,?5", new Object[]{
                            complementBien.getTypeComplement().getCode().trim(),
                            bien.getId().trim(),
                            complementBien.getValeur(),
                            agentID,
                            complementBien.getDevise()
                        });
                        counter++;
                    } else {
                        bulkQuery.put(counter + ":UPDATE T_COMPLEMENT_BIEN SET VALEUR = ?1, DEVISE = ?2 WHERE ID = ?3", new Object[]{
                            complementBien.getValeur(),
                            complementBien.getDevise(),
                            complementBien.getId().trim()
                        });
                        counter++;
                    }
                }
                result = executeQueryBulkInsert(bulkQuery);
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveBien(Bien bien, Acquisition acquisition, List<ComplementBien> complementBiens, Integer agentID, String type) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        String query;

        try {

            if (bien.getId() == null || bien.getId().isEmpty()) {

                String returnValue = "BIEN_ID";

                params.put("INTITULE", bien.getIntitule().trim());
                params.put("DESCRIPTION", bien.getDescription().trim());
                params.put("PERSONNE", acquisition.getPersonne().getCode());
                params.put("ADRESSE", bien.getFkAdressePersonne().getCode());
                params.put("TYPE_BIEN", bien.getTypeBien().getCode());
                params.put("AGENT_CREAT", agentID);
                params.put("DATE_ACQUISITION", acquisition.getDateAcquisition());

                if (type.equals("2")) {
                    query = "F_NEW_BIEN_IMMOBILIER_WEB";
                    params.put("FK_USAGE_BIEN", bien.getFkUsageBien());
                    params.put("FK_TARIF", bien.getFkTarif());
                    params.put("FK_COMMUNE", bien.getFkCommune());
                    params.put("FKQUARTIER", bien.getFkQuartier());
                } else {
                    query = "F_NEW_BIEN_WEB";
                    params.put("CATEGORIE", bien.getFkTarif());
                }

                for (ComplementBien complementBien : complementBiens) {
                    bulkQuery.put(counter + ":EXEC F_NEW_COMPLEMENT_BIEN ?1,'%s',?2,?3,?4", new Object[]{
                        complementBien.getTypeComplement().getCode().trim(),
                        complementBien.getValeur(),
                        agentID,
                        complementBien.getDevise()
                    });
                    counter++;
                }

                result = myDao.getDaoImpl().execBulkQuery(query, returnValue, params, bulkQuery);
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static TypeBienService getTypeBienServiceByTypeBien(String codeypeBien) {
        try {
            String query = "SELECT * FROM T_TYPE_BIEN_SERVICE WITH (READPAST) WHERE TYPE_BIEN = ?1 AND ETAT = 1";
            List<TypeBienService> typeBienServices = (List<TypeBienService>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBienService.class), false, codeypeBien);
            return typeBienServices.isEmpty() ? null : typeBienServices.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> getBiensOfPersonneV3(String codePersonne) {
        try {
            String query = "SELECT A.* FROM T_ACQUISITION A WITH (READPAST) WHERE A.PERSONNE = ?1 AND A.ETAT = 1";
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static UsageBien getUsageBienByCode(int id) {
        try {
            String query = "SELECT * FROM T_USAGE_BIEN WITH (READPAST) WHERE ID = ?1";
            List<UsageBien> usageBiens = (List<UsageBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(UsageBien.class), false, id);
            return usageBiens.isEmpty() ? null : usageBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static ValeurPredefinie getValeurPredefinieByCode(String codeVP, String typeComplement) {
        try {
            String query = "select * from T_VALEUR_PREDEFINIE WITH (READPAST) WHERE CODE = ?1 AND FK_TYPE_COMPLEMENT = ?2 AND ETAT = 1";
            List<ValeurPredefinie> valeurPredefinies = (List<ValeurPredefinie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ValeurPredefinie.class), false, codeVP, typeComplement);
            return valeurPredefinies.isEmpty() ? null : valeurPredefinies.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getResponsableBien(String idBien) {
        try {
            String query = "SELECT * FROM T_PERSONNE WITH (READPAST) WHERE CODE IN (SELECT DISTINCT PERSONNE FROM T_ACQUISITION WHERE BIEN = ?1 AND PROPRIETAIRE = 1 AND ETAT = 1)";
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, idBien);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Assujeti> getAssujettissementByPersonne(String codePersonne, String codeService) {
        try {
            String query = "SELECT A.* FROM T_ASSUJETI A WITH (READPAST) INNER JOIN T_BIEN B WITH (READPAST) ON A.BIEN = B.ID WHERE A.PERSONNE = ?1 AND A.ETAT = 1"
                    + " AND B.TYPE_BIEN IN (SELECT TYPE_BIEN FROM T_TYPE_BIEN_SERVICE WHERE SERVICE = ?2 AND ETAT = 1)";
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class),
                    false,
                    codePersonne, codeService);
            return assujetis;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Assujeti> getAssujettissementByPersonneV2(String codePersonne) {
        try {
            String query = "SELECT A.* FROM T_ASSUJETI A WITH (READPAST) WHERE A.PERSONNE = ?1 AND A.ETAT = 1";
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false, codePersonne);
            return assujetis;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String getUniteComplement(String code) {
        String sqlQuery = "SELECT  DBO.F_GET_UNITE2('%s')";
        sqlQuery = String.format(sqlQuery, code);
        String unite = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return unite;
    }

    public static AbComplementBien getAbComplementBienByArticleBudgetaire(String codeAB) {
        try {
            String query = "SELECT * FROM T_AB_COMPLEMENT_BIEN WITH (READPAST) "
                    + " WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1";
            List<AbComplementBien> abComplementBiens = (List<AbComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AbComplementBien.class), false, codeAB);
            return abComplementBiens.isEmpty() ? null : abComplementBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Palier getFistPalierByArticleBudgetaireV2(String codeAB, String codeFormeJuridique) {
        try {
            String query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2";
            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, codeFormeJuridique);
            return paliers.isEmpty() ? null : paliers.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static NoteCalcul getNoteCalculByNumero(String numeroNc) {
        String sqlQuery = "SELECT * FROM T_NOTE_CALCUL WITH (READPAST) WHERE NUMERO = ?1";
        List<NoteCalcul> noteCalculs = (List<NoteCalcul>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NoteCalcul.class), false, numeroNc.trim());
        return noteCalculs.isEmpty() ? null : noteCalculs.get(0);
    }

    public static BigDecimal getMontantTotalDuDetailNoteCalculv2(String noteCalcul) {

        String sqlParam = "('".concat(noteCalcul.trim()).concat("')");
        String sqlQuery = "SELECT dbo.F_GET_TOTAL_MONTANT_DU_DNC %s";
        sqlQuery = String.format(sqlQuery, sqlParam);
        BigDecimal result = myDao.getDaoImpl().getSingleResultToBigDecimal2(sqlQuery);
        return result;
    }

    public static boolean updatePasswordV2(String idUser, String password) {

        boolean result;

        String query = "UPDATE T_LOGIN_WEB SET PASSWORD = HASHBYTES ('MD5','" + password.trim() + "'), ETAT = 1 WHERE USERNAME = ?1";

        try {
            result = myDao.getDaoImpl().executeNativeQuery(query, idUser);
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static boolean createAcquisition(Acquisition acquisition) {

        String query = "EXEC F_NEW_ACQUISITION_BIEN ?1,?2,?3,?4,?5,?6,?7";

        boolean result = myDao.getDaoImpl().executeStoredProcedure(
                query,
                acquisition.getPersonne().getCode(),
                acquisition.getBien().getId(),
                acquisition.getDateAcquisition(),
                acquisition.getProprietaire(),
                acquisition.getReferenceContrat(),
                acquisition.getNumActeNotarie(),
                acquisition.getDateActeNotarie());
        return result;
    }

    public static Acquisition getAcquisistionByProprietaire(String proprietaire, String codeBien) {
        try {
            String query = "SELECT * FROM T_ACQUISITION WITH(READPAST) WHERE PERSONNE = ?1 AND BIEN = ?2 AND PROPRIETAIRE = 1 AND ETAT = 1";
            List<Acquisition> acquisition = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, proprietaire, codeBien);
            return acquisition.isEmpty() ? null : acquisition.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveAcquisition(Acquisition acquisition, String idAcquisition) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_NEW_ACQUISITION_BIEN ?1,?2,?3,?4,?5,?6,?7", new Object[]{
                acquisition.getPersonne().getCode(),
                acquisition.getBien().getId(),
                acquisition.getDateAcquisition(),
                acquisition.getProprietaire(),
                acquisition.getReferenceContrat(),
                acquisition.getNumActeNotarie(),
                acquisition.getDateActeNotarie()

            });
            counter++;

            bulkQuery.put(counter + ":UPDATE T_ACQUISITION WITH (READPAST) SET ETAT = 0, PROPRIETAIRE = 0 WHERE ID = ?1", new Object[]{
                idAcquisition
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean deleteBien(String idBien) {

        boolean result;

        String query = "UPDATE T_ACQUISITION WITH (READPAST) SET ETAT = 0, PROPRIETAIRE = 0 WHERE ID = ?1";

        try {
            result = myDao.getDaoImpl().executeNativeQuery(query, idBien);
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static List<ArticleBudgetaire> getArticleBudgetaireAssujettissable(String code) {

        try {

            String query = "SELECT * FROM T_ARTICLE_BUDGETAIRE WHERE CODE %s AND ETAT = 1";
            query = String.format(query, code);

            List<ArticleBudgetaire> articleBudgetaires = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, code);

            return articleBudgetaires;

        } catch (Exception e) {
            throw e;
        }
    }

    public static Bien getBienById(String idBien) {
        try {
            String query = "SELECT * FROM T_BIEN WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
            List<Bien> biens = (List<Bien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bien.class), false, idBien);
            return biens.isEmpty() ? null : biens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static ComplementBien getComplementBien(String codeTypeComplement, String idBien) {
        try {
            String query = "SELECT * FROM T_COMPLEMENT_BIEN WITH (READPAST) WHERE TYPE_COMPLEMENT =?1 AND BIEN =?2 AND ETAT= 1";
            List<ComplementBien> complementBiens = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class),
                    false,
                    codeTypeComplement,
                    idBien);
            return complementBiens.isEmpty() ? null : complementBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<AbComplementBien> getAssujettissableAbByBien(String idBien) {
        try {
            String query = "SELECT * FROM T_AB_COMPLEMENT_BIEN WITH (READPAST) WHERE TYPE_COMPLEMENT_BIEN "
                    + " IN (SELECT TCB.CODE FROM T_TYPE_COMPLEMENT_BIEN TCB WITH (READPAST) WHERE "
                    + " TCB.TYPE_BIEN = (SELECT B.TYPE_BIEN FROM T_BIEN B WITH (READPAST) WHERE B.ID = ?1 AND B.ETAT = 1 )AND TCB.ETAT = 1) AND ETAT = 1";
            List<AbComplementBien> abComplementBiens = (List<AbComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AbComplementBien.class), false, idBien);
            return abComplementBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Palier getFistPalierByArticleBudgetaire(String codeAB) {
        try {
            String query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2";
            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB);
            return paliers.isEmpty() ? null : paliers.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Palier> getListPaliersByAB(String codeAB, String type, String tarif) {
        try {

            String query = tarif.isEmpty() ? "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2" : "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND ETAT = 1 AND TYPE_PERSONNE = ?3 AND FK_ENTITE_ADMINISTRATIVE = ?4";

            List<Palier> paliers;

            if (tarif.isEmpty()) {
                paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type);
            } else {
                paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, tarif, type);
            }

            return paliers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Periodicite> getListPeriodicites() {
        try {
            String query = "SELECT * FROM T_PERIODICITE WITH (READPAST) WHERE ETAT = 1";
            List<Periodicite> periodicites = (List<Periodicite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Periodicite.class), false);
            return periodicites;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Bien getBienByCode(String code) {
        try {

            String query = "SELECT * FROM T_BIEN WITH (READPAST) WHERE ID = ?1";

            List<Bien> typeBiens = (List<Bien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bien.class), false, code.trim());
            return typeBiens.isEmpty() ? null : typeBiens.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Palier> getListPaliersByABV2(String codeAB, String type, String tarif, String codeQuartier, String typeBien) {
        try {

            String query = tarif.isEmpty() ? "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2" : "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND ETAT = 1 AND TYPE_PERSONNE = ?3 AND FK_ENTITE_ADMINISTRATIVE = ?4";

            List<Palier> paliers = new ArrayList<>();

            if (tarif.isEmpty()) {

                paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, codeQuartier);
            } else {

                switch (codeAB) {

                    case "00000000000002302020": // VIGNETTE

                        switch (typeBien) {

                            case "TB00000037":
                            case "TB00000038":
                            case "TB00000039":

                                query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND FK_TYPE_BIEN = ?2";

                                paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                        Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, typeBien);
                                break;
                            default:

                                query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3";

                                paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                        Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, tarif);

                                break;
                        }

                        break;

                    case "00000000000002282020": // RL
                    case "00000000000002292020": // IRL

                        query = "SELECT TOP 1 * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB);

//                        query = "SELECT top 1 * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND ETAT = 1";
//
//                        paliers = (List<Palier>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(Palier.class), false,
//                                codeAB.trim(), tarif);
                        break;
                    case "00000000000002272020": // IF

                        //if(typeBien.equals(paliers))
                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, tarif, typeBien);

                        break;
                    case "00000000000002312021": // ICM
                    case "00000000000002102016": // PUBLICITE

                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TARIF = ?2";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, tarif);

                        break;
                }
            }

            return paliers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Assujeti getExistAssujettissement(String idBien, String codeAB, String idComplementBien, String codePersonne, String tarif) {

        String query;
        List<Assujeti> assujetis;

        if (idBien.equals(GeneralConst.EMPTY_STRING) || idBien.equals(GeneralConst.NumberString.ZERO)) {

            query = "SELECT * FROM  T_ASSUJETI WITH (READPAST) "
                    + " WHERE ARTICLE_BUDGETAIRE =?1 AND PERSONNE = ?2 AND ETAT=1";
            assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false,
                    codeAB,
                    codePersonne, tarif);

        } else {

            query = "SELECT * FROM  T_ASSUJETI WITH (READPAST) WHERE BIEN =?1 "
                    + " AND ARTICLE_BUDGETAIRE =?2 AND PERSONNE =?3 AND ETAT=1";
            assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false,
                    idBien,
                    codeAB,
                    codePersonne);
        }

        try {

            return assujetis.isEmpty() ? null : assujetis.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveAssujettissement(Assujeti assujeti, List<PeriodeDeclaration> periodeDeclarations, JSONArray selectedAB, LogUser logUser) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = "F_NEW_ASSUJETISSEMENT";
            String returnValue = "ASSUJETTISSEMENT_ID";

            String codeAdressePersonne;
            if (assujeti.getFkAdressePersonne().getCode().equals(GeneralConst.EMPTY_STRING)) {
                AdressePersonne adressePersonne = DataAccess.getDefaultAdressePersonneByPersonne(assujeti.getPersonne().getCode());
                if (adressePersonne == null) {
                    codeAdressePersonne = null;
                } else {
                    codeAdressePersonne = adressePersonne.getCode();
                }
            } else {
                codeAdressePersonne = assujeti.getFkAdressePersonne().getCode();
            }

            String idBien = assujeti.getBien().getId().equals(GeneralConst.NumberString.ZERO)
                    ? null : assujeti.getBien().getId();

//            String macAdress = logUser.getMacAddress().trim();
//            String getIpAddress = logUser.getIpAddress().trim();
//            String getHostName = logUser.getHostName().trim();
//            String getEvent = logUser.getEvent().trim();
//            String getBrowserContext = logUser.getBrowserContext().trim();
            params.put("BIEN", idBien);
            params.put("ARTICLE_BUDGETAIRE", assujeti.getArticleBudgetaire().getCode());
            params.put("DUREE", 1);
            params.put("RENOUVELLEMENT", assujeti.getRenouvellement());
            params.put("DATE_DEBUT", assujeti.getDateDebut());
            params.put("DATE_FIN", assujeti.getDateFin());
            params.put("VALEUR", assujeti.getValeur());
            params.put("PERSONNE", assujeti.getPersonne().getCode());
            params.put("ADRESSE", codeAdressePersonne);
            params.put("NBR_JOUR_LIMITE", assujeti.getNbrJourLimite());
            params.put("PERIODICITE", assujeti.getPeriodicite());
            params.put("TARIF", assujeti.getTarif().getCode());
            params.put("GESTIONNAIRE", assujeti.getGestionnaire());
            params.put("COMPLEMENT_BIEN", assujeti.getComplementBien());
            params.put("AGENT_CREAT", "");
            params.put("NOUVELLES_ECHEANCES", "");
            params.put("UNITE_VALEUR", assujeti.getUniteValeur());
            params.put(GeneralConst.LogParam.MAC_ADRESSE, "");
            params.put(GeneralConst.LogParam.IP_ADRESSE, "");
            params.put(GeneralConst.LogParam.HOST_NAME, "");
            params.put(GeneralConst.LogParam.FK_EVENEMENT, "");
            params.put(GeneralConst.LogParam.CONTEXT_BROWSER, "");

            for (PeriodeDeclaration declaration : periodeDeclarations) {
                bulkQuery.put(counter + ":EXEC F_NEW_PERIODE_DECLARATION '%s',?1,?2,?3,?4,?5", new Object[]{
                    declaration.getDebut(),
                    declaration.getFin(),
                    declaration.getDateLimite(),
                    declaration.getDateLimitePaiement(),
                    null
                });
                counter++;
            }

            for (int i = 0; i < selectedAB.length(); i++) {

                JSONObject jsonobject = selectedAB.getJSONObject(i);
                String codeAB = jsonobject.getString("id");

                counter++;
//                bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_ASSUJ_AB, new Object[]{
//                    codeAB,});

            }

            result = myDao.getDaoImpl().execBulkQuery(query, returnValue, params, bulkQuery);
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static AdressePersonne getDefaultAdressePersonneByPersonne(String codePersonne) {
        try {
            String query = "SELECT * FROM T_ADRESSE_PERSONNE WITH (READPAST) "
                    + " WHERE PERSONNE = ?1 AND PAR_DEFAUT = 1";
            List<AdressePersonne> adressePersonnes = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, codePersonne);
            return adressePersonnes.isEmpty() ? null : adressePersonnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Site> getSitesPeage() {
        try {
            String query = "SELECT * FROM T_SITE WITH (READPAST) WHERE PEAGE = 1 AND ETAT = 1";
            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false);
            return sites;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListTarifsPeage() {

        try {

            String query = "SELECT * FROM T_TARIF WITH (READPAST) WHERE ETAT = 1 ORDER BY INTITULE";

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return tarifs;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TarifSite> getListTarifSitesPeage(int idAxe, String idCat, int isPeageLualaba) {
        try {
            String query = "SELECT * FROM T_TARIF_SITE WITH (READPAST) WHERE "
                    + "FK_SITE_DESTINANTION IN(SELECT FK_SITE FROM T_AXE_PEAGE WITH (READPAST) "
                    + "WHERE FK_AXE = ?1) AND FK_TARIF = ?2 AND ETAT = 1";
            if (isPeageLualaba == 1) {
                query = "SELECT * FROM T_TARIF_SITE WITH (READPAST) WHERE FK_SITE_PROVENANCE = ("
                        + "SELECT TOP 1 FK_SITE FROM T_AXE_PEAGE WITH (READPAST) WHERE FK_AXE = ?1) AND "
                        + "FK_SITE_DESTINANTION =(SELECT TOP 1 FK_SITE FROM T_AXE_PEAGE WITH (READPAST) "
                        + "WHERE FK_AXE = ?1 AND FK_SITE !=(SELECT TOP 1 FK_SITE FROM T_AXE_PEAGE WITH (READPAST) "
                        + "WHERE FK_AXE = ?1)) AND FK_TARIF = ?2 AND ETAT = 1";
            }
            List<TarifSite> tses = (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false, idAxe, idCat, isPeageLualaba);
            return tses;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveCommandeVoucher2(List<VoucherMock> vms) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> params = new HashMap<>();
        Boolean result = false;
        String codePersonne = GeneralConst.EMPTY_STRING,
                codeAb = GeneralConst.EMPTY_STRING,
                devise = GeneralConst.EMPTY_STRING,
                reference = GeneralConst.EMPTY_STRING,
                encoder = GeneralConst.EMPTY_STRING,
                compte_bancaire = GeneralConst.EMPTY_STRING;
        double total = 0;
        int agentCreate = 37, etat = 3;

        try {

            String firstStoredProcedure = "F_NEW_COMMANDE";
            String firstStoredReturnValueKey = "ID";

            String EXEC_F_CREATE_VOUCHER = ":EXEC F_CREATE_VOUCHER %s,?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12";

            for (VoucherMock vm : vms) {

                codePersonne = vm.getFk_personne();
                codeAb = vm.getFkAb();
                total += vm.getMontant();
                devise = vm.getDevise();
                reference = vm.getReference();
                compte_bancaire = vm.getCompteBancaire();
                agentCreate = vm.getAgentCreate();
                etat = vm.getEtat();
                encoder = Tools.encoder(Tools.generateRandomValue(10) + "@" + reference + "@" + Tools.generateRandomValue(50));

            }

            params.put("FK_PERSONNE", codePersonne);
            params.put("FK_AB", codeAb);
            params.put("MONTANT", total);
            params.put("DEVISE", devise);
            params.put("ETAT", etat);
            params.put("REFERENCE", reference);
            params.put("TOKEN", encoder);
            params.put("COMPTE_BANCAIRE", compte_bancaire);
            params.put("AGENT_CREATE", agentCreate);

            for (VoucherMock vm : vms) {

                firstBulk.put(counter + EXEC_F_CREATE_VOUCHER, new Object[]{
                    vm.getType(),
                    vm.getFkTarif(),
                    vm.getQuantite(),
                    vm.getPrix_unitaire(),
                    vm.getMontant(),
                    vm.getDevise(),
                    vm.getRemise(),
                    null,
                    3,
                    null
                });

                counter++;

            }

            result = myDao.getDaoImpl().execBulkQuery(firstStoredProcedure, firstStoredReturnValueKey, params, firstBulk);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Boolean saveCommandeGoPass(List<VoucherMock> vms, int qtePlage) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> params = new HashMap<>();
        Boolean result = false;
        String codePersonne = GeneralConst.EMPTY_STRING,
                codeAb = GeneralConst.EMPTY_STRING,
                devise = GeneralConst.EMPTY_STRING,
                reference = GeneralConst.EMPTY_STRING,
                encoder = GeneralConst.EMPTY_STRING,
                compte_bancaire = GeneralConst.EMPTY_STRING,
                idCmd;
        double total = 0;
        int agentCreate = 37, etat = 2;

        try {

            String firstStoredProcedure = "F_NEW_COMMANDE";
            String firstStoredReturnValueKey = "ID";

            String EXEC_F_NEW_GOPASS = ":EXEC F_NEW_GOPASS %s,?1,?2,?3,?4,?5,?6,?7,?8,?9";

            for (VoucherMock vm : vms) {

                codePersonne = vm.getFk_personne();
                codeAb = vm.getFkAb();
                total += vm.getMontant();
                devise = vm.getDevise();
                reference = vm.getReference();
                compte_bancaire = vm.getCompteBancaire();
                agentCreate = vm.getAgentCreate();
                etat = vm.getEtat();
                encoder = Tools.encoder(Tools.generateRandomValue(10) + "@" + reference + "@" + Tools.generateRandomValue(50));

            }

            params.put("FK_PERSONNE", codePersonne);
            params.put("FK_AB", codeAb);
            params.put("MONTANT", total);
            params.put("DEVISE", devise);
            params.put("ETAT", etat);
            params.put("REFERENCE", reference);
            params.put("TOKEN", encoder);
            params.put("COMPTE_BANCAIRE", compte_bancaire);
            params.put("AGENT_CREATE", agentCreate);

            for (VoucherMock vm : vms) {

                int n = vm.getQuantite() / qtePlage;
                double rest = vm.getQuantite() % qtePlage;

                if ((vm.getQuantite() / qtePlage) > 0) {
                    idCmd = DataAccess.getNewid();
                } else {
                    idCmd = null;
                }

                if (rest != 0) {

                    firstBulk.put(counter + EXEC_F_NEW_GOPASS, new Object[]{
                        vm.getType(),
                        vm.getFkTarif(),
                        rest,
                        vm.getPrix_unitaire(),
                        vm.getPrix_unitaire() * rest,
                        vm.getDevise(),
                        null,
                        vm.getEtat(),
                        idCmd
                    });

                    counter++;

                }

                for (int i = 0; i < n; i++) {

                    firstBulk.put(counter + EXEC_F_NEW_GOPASS, new Object[]{
                        vm.getType(),
                        vm.getFkTarif(),
                        qtePlage,
                        vm.getPrix_unitaire(),
                        vm.getPrix_unitaire() * qtePlage,
                        vm.getDevise(),
                        null,
                        vm.getEtat(),
                        idCmd
                    });

                    counter++;

                }
            }

            result = myDao.getDaoImpl().execBulkQuery(firstStoredProcedure, firstStoredReturnValueKey, params, firstBulk);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Boolean saveCommandeGoPassSpontanee(CommandeGoPassSpontanneeMock cgpsm) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> params = new HashMap<>();
        Boolean result = false;

        try {

            String firstStoredProcedure = "F_NEW_COMMANDE";
            String firstStoredReturnValueKey = "ID";

            String EXEC_F_NEW_GOPASS = ":EXEC F_NEW_GOPASS %s,?1,?2,?3,?4,?5,?6,?7,?8,?9";
            String EXEC_P_SAVE_TICKET_PEAGE_V2 = ":EXEC P_SAVE_TICKET_PEAGE_V2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14";

            params.put("FK_PERSONNE", null);
            params.put("FK_AB", cgpsm.getArticleBudgetaire());
            params.put("MONTANT", cgpsm.getMontant());
            params.put("DEVISE", cgpsm.getDevise());
            params.put("ETAT", 1);
            params.put("REFERENCE", cgpsm.getReference());
            params.put("TOKEN", cgpsm.getToken());
            params.put("COMPTE_BANCAIRE", cgpsm.getCompteBancaire());
            params.put("AGENT_CREATE", cgpsm.getAgentCreat());

            firstBulk.put(counter + EXEC_F_NEW_GOPASS, new Object[]{
                cgpsm.getTypeGopassCode(),
                cgpsm.getCategorieCode(),
                cgpsm.getQuantite(),
                cgpsm.getMontant(),
                cgpsm.getMontant(),
                cgpsm.getDevise(),
                cgpsm.getAgentCreat(),
                4,//cgpsm.getEtat(),
                null
            });

            counter++;

            firstBulk.put(counter + EXEC_P_SAVE_TICKET_PEAGE_V2, new Object[]{
                cgpsm.getCodeTicket(),
                null,
                null,
                cgpsm.getArticleBudgetaire(),
                cgpsm.getMontant(),
                cgpsm.getDevise(),
                cgpsm.getTypePaiement(),
                cgpsm.getMotifAnnulation(),
                cgpsm.getDateProd(),
                cgpsm.getAgentCreat(),
                cgpsm.getEtat(),
                null,
                cgpsm.getCategorieCode(),
                null
            });
            counter++;
            result = myDao.getDaoImpl().execBulkQuery(firstStoredProcedure, firstStoredReturnValueKey, params, firstBulk);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Boolean synchronisationGoPassSpontanee(CommandeGoPassSpontanneeMock cgpsm) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;
        Boolean result = false;

        try {

            String EXEC_P_SAVE_TICKET_PEAGE_V2 = ":EXEC P_SAVE_TICKET_PEAGE_V2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14";

            bulkQuery.put(counter + EXEC_P_SAVE_TICKET_PEAGE_V2, new Object[]{
                cgpsm.getCodeTicket(),
                null,
                null,
                cgpsm.getArticleBudgetaire(),
                cgpsm.getMontant(),
                cgpsm.getDevise(),
                cgpsm.getTypePaiement(),
                cgpsm.getMotifAnnulation(),
                cgpsm.getDateProd(),
                cgpsm.getAgentCreat(),
                cgpsm.getEtat(),
                null,
                cgpsm.getCategorieCode(),
                null
            });
            counter++;
            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static List<Commande> getCommandesGoPass(String nif) {
        try {
            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE FK_PERSONNE = ?1";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, nif);
            return commandes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Commande getListComandesGoPassByReference(String reference) {
        try {
            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE REFERENCE = ?1";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, reference);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Voucher> getListVoucher(int id_cmd) {
        try {
            String query = "SELECT * FROM T_VOUCHER WITH (READPAST) WHERE FK_COMMANDE = ?1";
            List<Voucher> vouchers = (List<Voucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Voucher.class), false, id_cmd);
            return vouchers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailCommande> getDetailGoPass(int id_cmd) {
        try {
            String query = "SELECT CHECKSUM(NEWID()) id, t.INTITULE tarif, TypeGopasse type, PrixUnitaire as prixUnitaire, "
                    + "SUM(Quantite) quantite, SUM(Montant) montant, devise FROM T_GO_PASSE g "
                    + "INNER JOIN T_TARIF t ON g.FK_TARIF = t.CODE WHERE FK_COMMANDE = ?1 "
                    + "GROUP BY NEW_ID, t.INTITULE, PrixUnitaire, devise, TypeGopasse ORDER BY quantite DESC";
            List<DetailCommande> dvs = (List<DetailCommande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailCommande.class), false, id_cmd);
            return dvs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailCommande getDetailVoucherByCommande(String reference) {
        try {
            String query = "SELECT CHECKSUM(NEWID()) id, a.INTITULE axe, a.SIGLE sigle, t.INTITULE tarif, "
                    + "PRIX_UNITAIRE as prixUnitaire, SUM(QTE) quantite, SUM(MONTANT) montant, "
                    + "SUM(MONTANT_FINAL) montantFinal, remise, devise FROM T_VOUCHER v INNER JOIN T_AXE a "
                    + "ON v.FK_AXE = a.ID INNER JOIN T_TARIF t ON v.FK_TARIF = t.CODE WHERE FK_COMMANDE = ("
                    + "SELECT ID FROM T_COMMANDE WHERE REFERENCE = ?1) GROUP BY NEW_ID, REMISE, a.INTITULE, "
                    + "a.SIGLE, t.INTITULE, PRIX_UNITAIRE, devise";
            List<DetailCommande> dvs = (List<DetailCommande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailCommande.class), false, reference);
            return dvs.isEmpty() ? null : dvs.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Commande getCmdAvailableToPay(String reference) {

        String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE REFERENCE = ?1 AND ETAT = 2";

        try {
            List<Commande> cvs = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, reference);
            return cvs.isEmpty() ? null : cvs.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean setEtatPayCommande(String reference, String ref) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC P_SET_ETAT_PAY_COMAMNDE ?1,?2", new Object[]{
                reference, ref
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<DetailVoucher> getListDetailVoucher(String nif) {
        try {
            String query = "SELECT * FROM T_DETAIL_VOUCHER WITH (READPAST) WHERE FK_VAUCHER IN(SELECT ID FROM T_VOUCHER	"
                    + "WHERE FK_COMMANDE IN(SELECT ID FROM T_COMMANDE WHERE FK_PERSONNE = ?1))";
            List<DetailVoucher> vouchers = (List<DetailVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailVoucher.class), false, nif);
            return vouchers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailsGoPass> getDetailsGoPassByCommandeId(int id) {
        try {
            String query = "SELECT * FROM T_DETAILS_GOPASSE WITH (READPAST) WHERE Fk_Gopasse IN("
                    + "SELECT IdGopasse FROM T_GO_PASSE WHERE FK_COMMANDE = ?1)";
            List<DetailsGoPass> dgps = (List<DetailsGoPass>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsGoPass.class), false, id);
            return dgps;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Commande getCommandesVoucher(String nif, String reference) {
        try {
            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE FK_PERSONNE = ?1 AND REFERENCE = ?2";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, nif, reference);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean setPreuvePaiementCommande2(int id, String base64) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC P_SAVE_PAYMENT_COMMAND_WITH_PROOF ?1,?2", new Object[]{
                id, base64
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Voucher> getVoucherByCommande(String reference) {
        try {
            String query = "SELECT * FROM T_VOUCHER WHERE FK_COMMANDE = (SELECT ID FROM T_COMMANDE WHERE REFERENCE = ?1)";
            List<Voucher> vouchers = (List<Voucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Voucher.class), false, reference);
            return vouchers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean generateGoPass(int fkGoPass, String codeTicketGoPass) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_GENERATE_GOPASS ?1,?2", new Object[]{
                fkGoPass, codeTicketGoPass
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean generateGoPassSpontannee(int fkGoPass, CommandeGoPassSpontanneeMock cgpsm, String codeGoPass) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_GENERATE_GOPASS_SPONTANNEE ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12", new Object[]{
                fkGoPass,
                cgpsm.getPassager(),
                cgpsm.getPieceIdentite(),
                cgpsm.getSexePassager(),
                codeGoPass,
                cgpsm.getAgentCreat(),
                cgpsm.getAgentUtilisation(),
                cgpsm.getEtat(),
                cgpsm.getCodeTicket(),
                cgpsm.getCodeCompagnie(),
                cgpsm.getAeroportProvenance(),
                cgpsm.getAeroportDestination()
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean livrerVoucher(String reference) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_LIVRER_VOUCHER ?1", new Object[]{
                reference
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean savePreuvePaiementCommande(int id, String base64) {

        boolean result;

        String query = "UPDATE T_COMMANDE SET PREUVE_PAIEMENT = ?2 WHERE ID = ?1";

        try {
            result = myDao.getDaoImpl().executeNativeQuery(query, id, base64);
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static RemiseSousProvisionPeage getRemise(String nif, int idAxe, String idTarif) {
        try {
            String query = "SELECT * FROM T_REMISE_SOUS_PROVISION_PEAGE WITH (READPAST) WHERE FK_PERSONNE = ?1 AND FK_AXE = ?2 AND FK_TARIF = ?3";
            List<RemiseSousProvisionPeage> commandes = (List<RemiseSousProvisionPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RemiseSousProvisionPeage.class), false, nif, idAxe, idTarif);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveCommandeCartes(CommandeCarte commandeCarte) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_NEW_COMMANDE_CARTE ?1,?2,?3,?4,?5,?6,?7,?8", new Object[]{
                commandeCarte.getFkPersonne().getCode(),
                commandeCarte.getQte(),
                commandeCarte.getPrixUnitaire(),
                commandeCarte.getPrixTotal(),
                commandeCarte.getEtat(),
                commandeCarte.getReference(),
                commandeCarte.getDevise(),
                Tools.encoder(Tools.generateRandomValue(10) + "@" + commandeCarte.getReference() + "@" + Tools.generateRandomValue(50))

            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<CommandeCarte> getListComandesCartes(String nif) {
        try {
            String query = "SELECT * FROM T_COMMANDE_CARTE WITH (READPAST) WHERE FK_PERSONNE = ?1";
            List<CommandeCarte> commandes = (List<CommandeCarte>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CommandeCarte.class), false, nif);
            return commandes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean attributeCarteVoucher(int idCarte, List<String> vouchertList, int type) {

        try {

            String sqlQuery = GeneralConst.EMPTY_STRING;

            HashMap<String, Object[]> bulkQuery = new HashMap<>();
            int counter = 0;
            if (type == 1) {
                for (String voucherId : vouchertList) {
                    counter++;
                    sqlQuery = ":INSERT INTO T_CARTE_DETAIL_VOUCHER (FK_CARTE, FK_DETAIL_VOUCHER, DATE_CREATE, ETAT) Values (%s, %s, GETDATE(), 1)";
                    sqlQuery = String.format(sqlQuery, idCarte, Integer.valueOf(voucherId));
                    bulkQuery.put(counter + sqlQuery, new Object[]{});
                }
            } else {
                for (String voucherId : vouchertList) {
                    counter++;
                    sqlQuery = ":DELETE FROM T_CARTE_DETAIL_VOUCHER WHERE FK_CARTE = %s AND FK_DETAIL_VOUCHER = %s";
                    sqlQuery = String.format(sqlQuery, idCarte, Integer.valueOf(voucherId));
                    bulkQuery.put(counter + sqlQuery, new Object[]{});
                }
            }

            return DataAccess.executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<CarteVoucher> getListCartesVoucher(String nif) {
        try {
            String query = "SELECT * FROM T_CARTE_VOUCHER WITH (READPAST) WHERE FK_PERSONNE = ?1";
            List<CarteVoucher> cvs = (List<CarteVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CarteVoucher.class), false, nif);
            return cvs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailVoucher> getVouchersCarte(String nif, int idCarte, int type) {
        try {
            String query;

            if (type == 1) {
                query = "SELECT * FROM T_DETAIL_VOUCHER WITH (READPAST) WHERE ID NOT IN("
                        + "SELECT FK_DETAIL_VOUCHER FROM T_CARTE_DETAIL_VOUCHER ) AND "
                        + "FK_VAUCHER IN(SELECT ID FROM T_VOUCHER WITH (READPAST) WHERE FK_COMMANDE IN("
                        + "SELECT ID FROM T_COMMANDE WHERE FK_PERSONNE = ?1)) AND DATE_UTILISATION IS NULL";
            } else if (type == 2) {
                query = "SELECT * FROM T_DETAIL_VOUCHER WITH (READPAST) WHERE ID IN("
                        + "SELECT FK_DETAIL_VOUCHER FROM T_CARTE_DETAIL_VOUCHER ) AND "
                        + "FK_VAUCHER IN(SELECT ID FROM T_VOUCHER WITH (READPAST) WHERE FK_COMMANDE IN("
                        + "SELECT ID FROM T_COMMANDE WHERE WITH (READPAST) FK_PERSONNE = ?1)) AND DATE_UTILISATION IS NULL";
            } else {
                query = "SELECT * FROM T_DETAIL_VOUCHER WITH (READPAST) WHERE ID IN("
                        + "SELECT FK_DETAIL_VOUCHER FROM T_CARTE_DETAIL_VOUCHER WITH (READPAST) WHERE FK_CARTE = ?2) "
                        + "AND FK_VAUCHER IN(SELECT ID FROM T_VOUCHER WITH (READPAST) WHERE FK_COMMANDE IN("
                        + "SELECT ID FROM T_COMMANDE WHERE WITH (READPAST) FK_PERSONNE = ?1))";
            }

            List<DetailVoucher> dvs = (List<DetailVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailVoucher.class), false, nif, idCarte);
            return dvs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Object> getDataDashBoard(String datedebut, String datefin, String nif) {
        try {

            HashMap<String, Object> params = new HashMap<>();

            String query = "F_TABLEAU_DE_BORD_TELE_PROCEDURE";

            params.put("DateDebut", datedebut);
            params.put("DateFin", datefin);
            params.put("Nif", nif);

            List<Object> dashboard = (List<Object>) myDao.getDaoImpl().findStoredProcedure(query, params);
            return dashboard;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Object> getDataDashBoardPeage(String dateDebut, String dateFin, String nif, int isDateFiltre) {
        try {

            HashMap<String, Object> params = new HashMap<String, Object>();

            String query = "F_TABLEAU_DE_BORD_PEAGE";

            params.put("DateDebut", dateDebut);
            params.put("DateFin", dateFin);
            params.put("nif", nif);
            params.put("isDateFiltre", isDateFiltre);

            List<Object> dashboard = (List<Object>) myDao.getDaoImpl().findStoredProcedure(query, params);
            return dashboard;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Axe> getListAxes() {
        try {
            String query = "SELECT * FROM T_AXE WITH (READPAST) WHERE ETAT = 1";
            List<Axe> axes = (List<Axe>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Axe.class), false);
            return axes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<AxePeage> getListSiteAxePeage(int fkAxe) {
        try {
            String query = "SELECT * FROM T_AXE_PEAGE WITH (READPAST) WHERE FK_AXE = ?1 AND ETAT = 1";
            List<AxePeage> aps = (List<AxePeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AxePeage.class), false, fkAxe);
            return aps;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean updateUserInfos(LoginWeb lw) {

        boolean result;

        String query = "UPDATE T_LOGIN_WEB SET MAIL = ?1, TELEPHONE = ?2 WHERE FK_PERSONNE = ?3";

        try {
            result = myDao.getDaoImpl().executeNativeQuery(query, lw.getMail(), lw.getTelephone(), lw.getFkPersonne());
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationByCodeDeclarationV2(String code) {

        try {
            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE (ID = %s OR CODE_DECLARATION = ?1)";
            query = String.format(query, code);

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, code);
            return declarations;
        } catch (Exception e) {
            throw e;
        }
    }

    public static SiteBanque getSiteBanqueBySite(String codeSite) {
        try {
            String query = "SELECT * FROM T_SITE_BANQUE WITH (READPAST) WHERE SITE  = ?1 AND ETAT = 1";
            List<SiteBanque> siteBanques = (List<SiteBanque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(SiteBanque.class), false, codeSite);
            return siteBanques.isEmpty() ? null : siteBanques.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static AdressePersonne getDefaultAdressByPersone(String personCode) {
        try {
            String query = "SELECT * FROM T_ADRESSE_PERSONNE WITH(READPAST) "
                    + " WHERE PERSONNE = ?1 AND PAR_DEFAUT = 1 AND ETAT = 1";
            List<AdressePersonne> adresses = (List<AdressePersonne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AdressePersonne.class), false, personCode);
            return adresses.isEmpty() ? null : adresses.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static RetraitDeclaration getRetraitDeclarationByID(int id) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, id);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static PeriodeDeclaration getPeriodeDeclarationById(int periodeId) {
        String query = "SELECT * FROM T_PERIODE_DECLARATION WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<PeriodeDeclaration> declarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, periodeId);
        return declarations.isEmpty() ? null : declarations.get(0);
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationByNewID_V2(String newId) {
        try {

            String sqlQuery;

            sqlQuery = "SELECT ID, MONTANT,FK_PERIODE,FK_AB FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE NEW_ID = ?1 AND RETRAIT_DECLARATION_MERE IS NULL order by fk_Periode";

            List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class),
                    false, newId.trim());

            return retraitDeclarations;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static PeriodeDeclaration getPeriodeDeclarationByIdV2(int periodeId) {
        String query = "SELECT ID,DEBUT FROM T_PERIODE_DECLARATION  WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";
        List<PeriodeDeclaration> declarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, periodeId);
        return declarations.isEmpty() ? null : declarations.get(0);
    }

    public static ArticleBudgetaire getArticleBudgetaireByCodeV2(String codeAB) {

        try {
            String query = "SELECT CODE,PERIODICITE,INTITULE,CODE_OFFICIEL FROM T_ARTICLE_BUDGETAIRE WITH (READPAST) WHERE CODE = ?1";

            List<ArticleBudgetaire> articleBudg = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeAB);
            return articleBudg.size() > 0 ? articleBudg.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Bordereau getBordereauByDeclaration(String codeDeclaration) {
        try {
            String query = "SELECT B.* FROM T_BORDEREAU B WITH (READPAST) WHERE B.NUMERO_DECLARATION = ?1";
            List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bordereau.class), false, codeDeclaration);
            return bordereaus.isEmpty() ? null : bordereaus.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Commande> getListCommandesPaid(String codeSite, String compteBancaire, String dateDebut, String dateFin) {
        try {

            String sqlQueryAccountBank = "";

            if (!compteBancaire.equals("*")) {
                sqlQueryAccountBank = " AND C.FK_COMPTE_BANCAIRE = '%s'";
                sqlQueryAccountBank = String.format(sqlQueryAccountBank, compteBancaire);
            }

            String sqlQueryDates = " AND DBO.FDATE(C.DATE_PAIEMENT) BETWEEN '%s' AND '%s'";
            sqlQueryDates = String.format(sqlQueryDates, dateDebut, dateFin);

            String query = "SELECT C.* FROM T_COMMANDE C WITH (READPAST) WHERE C.SITE_PAIEMENT = ?1 %s %s";
            query = String.format(query, sqlQueryAccountBank, sqlQueryDates);

            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, codeSite);
            return commandes;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent getAgentByCode(int code) {
        try {

            String query = "SELECT * FROM T_AGENT WITH (READPAST) WHERE CODE = ?1";

            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, code);
            return agents.isEmpty() ? null : agents.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean savePaiementCommandeVoucher(String referenceDocument, String referencePaiement, int agentPaiement, String codeSite) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = ":UPDATE T_COMMANDE SET ETAT = 2, DATE_PAIEMENT = GETDATE(),REFERENCE_PAIEMENT = ?1,AGENT_PAIEMENT = ?2"
                    + "  WHERE REFERENCE  = ?1";

            bulkQuery.put(counter + query, new Object[]{referenceDocument, agentPaiement});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
        }
        return result;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationByCodeDeclaration(String code) {

        try {
            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE CODE_DECLARATION = ?1";
            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, code);
            return declarations;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Commande verifyCommandeByReference(String reference) {

        try {
            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE REFERENCE = ?1";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, reference);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean checkReferePaiementExists(String numeroBordereau, String compteBancaire, String typeDocument) {
        boolean exist = true;
        String sqlQuery = GeneralConst.EMPTY_STRING;

        switch (typeDocument) {
            case "DEC":

                sqlQuery = "SELECT * FROM T_BORDEREAU WITH (READPAST) "
                        + " WHERE NUMERO_BORDEREAU = ?1 AND COMPTE_BANCAIRE = ?2";

                List<Bordereau> bordereaus = (List<Bordereau>) myDao.getDaoImpl().find(sqlQuery,
                        Casting.getInstance().convertIntoClassType(Bordereau.class), false,
                        numeroBordereau.trim(), compteBancaire.trim());

                if (bordereaus != null) {
                    if (bordereaus.isEmpty()) {
                        exist = false;
                    }
                } else {
                    exist = false;
                }
                break;
            default:
                sqlQuery = "SELECT * FROM T_JOURNAL WITH (READPAST) "
                        + " WHERE BORDEREAU = ?1 AND COMPTE_BANCAIRE = ?2";

                List<Journal> journals = (List<Journal>) myDao.getDaoImpl().find(sqlQuery,
                        Casting.getInstance().convertIntoClassType(Journal.class), false,
                        numeroBordereau.trim(), compteBancaire.trim());

                if (journals != null) {
                    if (journals.isEmpty()) {
                        exist = false;
                    }
                } else {
                    exist = false;
                }
                break;
        }

        return exist;
    }

    public static boolean createDeclaration(Bordereau bordereau, List<RetraitDeclaration> listDeclaration, String deviseCode) {

        String bordereauID = GeneralConst.EMPTY_STRING;
        boolean result = false;

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        HashMap<String, Object> paramsBordereau = new HashMap<>();
        int counterNC = GeneralConst.Numeric.ZERO;

        String firstStoredProcedure = "F_NEW_BORDEREAU";
        String secondStoredReturnValueKey = "BORDEREAU";

        paramsBordereau.put("NUMERO_BORDEREAU", bordereau.getNumeroBordereau());
        paramsBordereau.put("NOM_COMPLET", bordereau.getNomComplet());
        paramsBordereau.put("DATE_PAIEMENT", new Date());
        paramsBordereau.put("TOTAL_MONTANT", bordereau.getTotalMontantPercu());
        paramsBordereau.put("COMPTE_BANCAIRE", bordereau.getCompteBancaire().getCode());
        paramsBordereau.put("AGENT", bordereau.getAgent().getCode());
        paramsBordereau.put("DATE_CREATE", new Date());
        paramsBordereau.put("ETAT", bordereau.getEtat().getCode());
        paramsBordereau.put("NUMERO_DECLARATION", bordereau.getNumeroDeclaration());
        paramsBordereau.put("NUMERO_ATTESTATION_PAIEMENT", GeneralConst.EMPTY_STRING);
        paramsBordereau.put("CENTRE", bordereau.getCentre());

        if (!listDeclaration.isEmpty()) {

            for (RetraitDeclaration retraitDeclaration : listDeclaration) {
                counterNC++;

                BigDecimal amount = new BigDecimal(0);

                if (retraitDeclaration.getEtat() == 4 || retraitDeclaration.getEtat() == 3) {

                    //amount = amount.add(retraitDeclaration.getMontant());
                    RetraitDeclaration rdPere = getRetraitDeclarationByID(retraitDeclaration.getRetraitDeclarationMere());
                    retraitDeclaration.setFkPeriode(rdPere.getFkPeriode());
                    amount = amount.add(bordereau.getTotalMontantPercu());

                } else {
                    amount = amount.add(bordereau.getTotalMontantPercu());
                }

                firstBulk.put(counterNC + ":EXEC F_NEW_DETAIL_BORDEREAU %s,?1,?2,?3,?4", new Object[]{
                    retraitDeclaration.getFkAb().trim(),
                    amount,
                    deviseCode,//retraitDeclaration.getDevise(),
                    retraitDeclaration.getFkPeriode()
                });
            }
        }

        try {
            bordereauID = myDao.getDaoImpl().execBulkQueryWithOneLevel(
                    firstStoredProcedure, secondStoredReturnValueKey,
                    paramsBordereau, firstBulk);

            if (!bordereauID.isEmpty()) {
                result = true;
            }

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static ParamGlobal getParamByIntitule(String intitule) {

        try {
            String query = "SELECT * FROM T_PARAM_GLOBAL WITH (READPAST) WHERE INTITULE = ?1";
            List<ParamGlobal> pgs = (List<ParamGlobal>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ParamGlobal.class), false, intitule);
            return pgs.isEmpty() ? null : pgs.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean setCommandeNotDelivered(String base64, String reference) {

        boolean result;

        String query = "UPDATE T_COMMANDE SET ETAT = 5, FICHIER_ZIP = ?1 WHERE REFERENCE = ?2";

        try {
            result = myDao.getDaoImpl().executeNativeQuery(query, base64, reference);
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static Commande getCommandesVoucherByReference(String reference) {
        try {
            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE REFERENCE = ?1";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, reference);
            return commandes.isEmpty() ? null : commandes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Commande> getCommandesVoucherNotDelivered() {
        try {
            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE ETAT = 5 AND FICHIER_ZIP IS NOT NULL";
            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false);
            return commandes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> rechercheAvanceeBien(String nif, String typeBien, String dateDebut, String dateFin) {
        try {

            String query = "SELECT * FROM T_ACQUISITION WHERE ETAT = 1 AND PERSONNE = ?1 "
                    + "AND BIEN IN(SELECT ID FROM T_BIEN WHERE TYPE_BIEN = ?2 AND "
                    + "CAST(DATE_CREATE AS date) >= ?3 AND CAST(DATE_CREATE AS date) <= ?4)";

            if (typeBien.equals("*d")) {
                query = "SELECT * FROM T_ACQUISITION WHERE ETAT = 1 AND PERSONNE = ?1 "
                        + "AND BIEN IN(SELECT ID FROM T_BIEN WHERE CAST(DATE_CREATE AS date) >= ?3 "
                        + "AND CAST(DATE_CREATE AS date) <= ?4)";
            }

            List<Acquisition> personnes = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, nif, typeBien, dateDebut, dateFin);

            return personnes;
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean generateVouchersWithDirectSwift(int id, String reference, double montantCmd, double montantPaye) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + ":EXEC F_CONCILIATION_VOUCHERS ?1,?2,?3,?4", new Object[]{
                id, reference, montantCmd, montantPaye
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean activateAccount(String codePersonne) {

        boolean result;

        String query = "UPDATE T_LOGIN_WEB SET ETAT =2 WHERE FK_PERSONNE = ?1";

        try {
            result = myDao.getDaoImpl().executeNativeQuery(query, codePersonne);
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public static TarifSite getTarifSite(String fkTarif) {

        try {

            String query = "SELECT * FROM T_TARIF_SITE WITH (READPAST) WHERE FK_TARIF = ?1 AND ETAT = 1";

            List<TarifSite> tarifSites = (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false, fkTarif);
            return tarifSites.isEmpty() ? null : tarifSites.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<GoPass> getGoPassByCommande(String reference) {
        try {
            String query = "SELECT * FROM T_GO_PASSE WHERE Fk_Commande = (SELECT ID FROM T_COMMANDE WHERE REFERENCE = ?1)";
            List<GoPass> gps = (List<GoPass>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(GoPass.class), false, reference);
            return gps;
        } catch (Exception e) {
            throw (e);
        }
    }

}
