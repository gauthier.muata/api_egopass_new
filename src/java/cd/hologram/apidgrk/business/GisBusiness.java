/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.business;

import static cd.hologram.apidgrk.business.DataAccess.getNC;
import static cd.hologram.apidgrk.business.DataAccess.getPeriodicite;
import cd.hologram.apidgrk.constants.GeneralConst;
import cd.hologram.apidgrk.constants.GisConst;
import cd.hologram.apidgrk.constants.PropertiesConst;
import cd.hologram.apidgrk.entities.Acquisition;
import cd.hologram.apidgrk.entities.AdressePersonne;
import cd.hologram.apidgrk.entities.ArticleBudgetaire;
import cd.hologram.apidgrk.entities.BanqueAb;
import cd.hologram.apidgrk.entities.Bien;
import cd.hologram.apidgrk.entities.ComplementBien;
import cd.hologram.apidgrk.entities.CompteBancaire;
import cd.hologram.apidgrk.entities.Devise;
import cd.hologram.apidgrk.entities.EntiteAdministrative;
import cd.hologram.apidgrk.entities.FormeJuridique;
import cd.hologram.apidgrk.entities.Journal;
import cd.hologram.apidgrk.entities.NoteCalcul;
import cd.hologram.apidgrk.entities.NotePerception;
import cd.hologram.apidgrk.entities.Periodicite;
import cd.hologram.apidgrk.entities.Personne;
import cd.hologram.apidgrk.entities.Tarif;
import cd.hologram.apidgrk.entities.TypeBien;
import cd.hologram.apidgrk.entities.Unite;
import cd.hologram.apidgrk.mock.BienCreate;
import cd.hologram.apidgrk.mock.ComplementsBienCreate;
import cd.hologram.apidgrk.mock.ComplementsPersonneMorale;
import cd.hologram.apidgrk.mock.ComplementsPersonnePhysique;
import cd.hologram.apidgrk.mock.LocationPaymentData;
import cd.hologram.apidgrk.mock.MockAdresse;
import cd.hologram.apidgrk.mock.MockArticleBudgetaire;

import cd.hologram.apidgrk.mock.MockBien;
import cd.hologram.apidgrk.mock.MockCompteBancaire;
import cd.hologram.apidgrk.mock.MockDashboardData;
import cd.hologram.apidgrk.mock.MockDetailProperty;
import cd.hologram.apidgrk.mock.MockDevise;
import cd.hologram.apidgrk.mock.MockEntiteAdministrative;
import cd.hologram.apidgrk.mock.MockPersonCreate;
import cd.hologram.apidgrk.mock.MockPersonne;
import cd.hologram.apidgrk.mock.MockPersonneLocation;
import cd.hologram.apidgrk.mock.MockRetraitDeclaration;
import cd.hologram.apidgrk.mock.MockTarif;
import cd.hologram.apidgrk.mock.MockTypeBien;
import cd.hologram.apidgrk.mock.MockTypePersonne;
import cd.hologram.apidgrk.mock.MockUnit;
import cd.hologram.apidgrk.mock.MockUnitsObject;
import cd.hologram.apidgrk.mock.PersonCreate;
import cd.hologram.apidgrk.mock.ResponseStatus;
import cd.hologram.apidgrk.mock.Success;
import cd.hologram.apidgrk.mock.Transaction;
import cd.hologram.apidgrk.properties.PropertiesConfig;
import cd.hologram.apidgrk.properties.PropertiesMessage;
import cd.hologram.apidgrk.utils.DateUtil;

import cd.hologram.apidgrk.utils.ResponseMessage;
import cd.hologram.apidgrk.utils.Tools;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author juslin.tshiamua
 */
public class GisBusiness implements Serializable {

    static DataAccess dataAccess;
    static ResponseMessage responseMessage;
    static PropertiesConfig propertiesConfig;
    static PropertiesMessage propertiesMessage;

    public GisBusiness() throws IOException {
        propertiesConfig = new PropertiesConfig();
        propertiesMessage = new PropertiesMessage();
        responseMessage = new ResponseMessage();
    }

    public static String getAdministrativeEntitiesByCity(
            String avenueCode,
            String districtCode,
            String communeCode,
            String filterByVille,
            String filterByDistrict,
            String filterByCommune
    ) throws IOException, JSONException {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();

        List<MockEntiteAdministrative> mockea = new ArrayList<>();

        List<Success> dataList = new ArrayList<>();
        List<ResponseStatus> rses = new ArrayList<>();
        ResponseStatus rs = new ResponseStatus();
        Success success = new Success();
        Gson gson = new Gson();
        dataAccess = new DataAccess();

        try {

            EntiteAdministrative cityObject = dataAccess.getAdministrativeEntities(avenueCode);

            if (cityObject != null) {

                rs.setCode(GisConst.status.SUCCESS_CODE);
                rs.setMessage(GisConst.status.SUCCESS_MESSAGE);

                List<MockEntiteAdministrative> mockea1 = new ArrayList<>();
                MockEntiteAdministrative mea = new MockEntiteAdministrative();

                mea.setCityCode(cityObject.getCode());
                mea.setCityName(cityObject.getIntitule());

                if (!cityObject.getEntiteAdministrativeList().isEmpty()) {

                    for (EntiteAdministrative distictObject : cityObject.getEntiteAdministrativeList()) {

                        List<MockEntiteAdministrative> mockea2 = new ArrayList<>();

                        MockEntiteAdministrative mea1 = new MockEntiteAdministrative();
                        mea1.setDistrictCode(distictObject.getCode());
                        mea1.setDistrictName(distictObject.getIntitule());
                        mea1.setCityCode(distictObject.getEntiteMere().getCode());

                        if (!distictObject.getEntiteAdministrativeList().isEmpty()) {

                            for (EntiteAdministrative communeObject : distictObject.getEntiteAdministrativeList()) {

                                List<MockEntiteAdministrative> mockea3 = new ArrayList<>();
                                MockEntiteAdministrative mea2 = new MockEntiteAdministrative();

                                mea2.setCommuneCode(communeObject.getCode());
                                mea2.setCommuneName(communeObject.getIntitule());
                                mea2.setDistrictCode(communeObject.getEntiteMere().getCode());

                                if (!communeObject.getEntiteAdministrativeList().isEmpty()) {
                                    for (EntiteAdministrative avenueObject : communeObject.getEntiteAdministrativeList()) {

                                        MockEntiteAdministrative mea3 = new MockEntiteAdministrative();
                                        mea3.setAvenueCode(avenueObject.getCode());
                                        mea3.setAvenueName(avenueObject.getIntitule());
                                        mea3.setCommuneCode(avenueObject.getEntiteMere().getCode());
                                        mockea3.add(mea3);

                                    }
                                }

                                mea2.setAvenueList(mockea3);
                                mockea2.add(mea2);
//
                            }
                        }
                        mea1.setCommuneList(mockea2);
                        mockea1.add(mea1);
                    }
                }
                mea.setDistrictList(mockea1);
                mockea.add(mea);

                rs.setAdministrativeEntityList(mockea);

            } else {
                rs.setCode(GisConst.status.EMPTY_CODE);
                rs.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            rses.add(rs);
            response = gson.toJson(rses);

        } catch (Exception e) {
            success.setCode(GisConst.status.ERROR_CODE);
            success.setMessage(GisConst.status.ERROR_MESSAGE);
            dataList.add(success);
            response = gson.toJson(dataList);
        }

        return response;
    }

    public static String getOwnerProperties(
            String communeId,
            String quarterId,
            String avenueId,
            String codeImmobilier
    ) {
        String response = GeneralConst.EMPTY_STRING;

        List<MockPersonne> listPersonne = new ArrayList<>();
        List<MockBien> listBien = new ArrayList<>();
        List<AdressePersonne> adressePersonnes = new ArrayList<>();
        List<Success> dataList = new ArrayList<>();
        Gson gson = new Gson();
        MockPersonne personne;
        MockBien bien = new MockBien();
        Success success = new Success();

        dataAccess = new DataAccess();

        String chaineAdresse = GeneralConst.EMPTY_STRING;
        String ville = GeneralConst.EMPTY_STRING;
        String commune = GeneralConst.EMPTY_STRING;
        String quartier = GeneralConst.EMPTY_STRING;
        String avenue = GeneralConst.EMPTY_STRING;
        String numero = GeneralConst.EMPTY_STRING;

        try {

            adressePersonnes = dataAccess.getAdressePersonne(communeId, quarterId, avenueId);
            EntiteAdministrative ea;

            if (!adressePersonnes.isEmpty()) {

                success.setCode(GisConst.status.SUCCESS_CODE);
                success.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (AdressePersonne ap : adressePersonnes) {

                    personne = new MockPersonne();

                    personne.setCode(ap.getPersonne().getCode());
                    personne.setPrenom(ap.getPersonne().getPrenoms() != null ? ap.getPersonne().getPrenoms() : GeneralConst.EMPTY_STRING);
                    personne.setPostnom(ap.getPersonne().getPostnom() != null ? ap.getPersonne().getPostnom() : GeneralConst.EMPTY_STRING);
                    personne.setNom(ap.getPersonne().getNom() != null ? ap.getPersonne().getNom() : GeneralConst.EMPTY_STRING);
                    personne.setNif(ap.getPersonne().getNif() != null ? ap.getPersonne().getNif() : GeneralConst.EMPTY_STRING);

                    if (ap.getPersonne() != null) {

                        if (ap.getAdresse() != null) {

                            ea = new EntiteAdministrative();
                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getVille());

                            if (ea != null) {
                                ville = ea.getIntitule().concat(" -->");
                            }

                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getCommune());

                            if (ea != null) {
                                commune = " C/".concat(ea.getIntitule());
                            }

                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getQuartier());

                            if (ea != null) {
                                quartier = " Q/".concat(ea.getIntitule());
                            }

                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getAvenue());

                            if (ea != null) {
                                avenue = " - Av. ".concat(ea.getIntitule());
                            }

                            numero = ap.getAdresse().getNumero() != null
                                    ? " n° ".concat(ap.getAdresse().getNumero())
                                    : GeneralConst.EMPTY_STRING;

                            chaineAdresse = ville.concat(commune).concat(quartier).concat(avenue).concat(numero);

                            personne.setAdresse(chaineAdresse);
                        }

                        if (!ap.getPersonne().getAcquisitionList().isEmpty()) {

                            for (Acquisition aq : ap.getPersonne().getAcquisitionList()) {

                                if (aq.getBien().getTypeBien().getCode().equals(codeImmobilier)) {

                                    if (aq.getBien().getEtat()) {

                                        bien = new MockBien();

                                        personne.setIsOwner(aq.getProprietaire());

                                        bien.setCode(aq.getBien().getId());
                                        bien.setIntitule(aq.getBien().getIntitule());
                                        bien.setDescription(aq.getBien().getDescription());

                                        if (aq.getBien().getFkAdressePersonne().getParDefaut()) {

                                            if (aq.getBien().getFkAdressePersonne().getAdresse() != null) {

                                                ea = new EntiteAdministrative();
                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getVille());

                                                if (ea != null) {
                                                    ville = ea.getIntitule().concat(" -->");
                                                }

                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getCommune());

                                                if (ea != null) {
                                                    commune = " C/".concat(ea.getIntitule());
                                                }

                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getQuartier());

                                                if (ea != null) {
                                                    quartier = " Q/".concat(ea.getIntitule());
                                                }

                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getAvenue());

                                                if (ea != null) {
                                                    avenue = " - Av. ".concat(ea.getIntitule());
                                                }

                                                numero = aq.getBien().getFkAdressePersonne().getAdresse().getNumero() != null
                                                        ? " n° ".concat(aq.getBien().getFkAdressePersonne().getAdresse().getNumero())
                                                        : GeneralConst.EMPTY_STRING;

                                                chaineAdresse = ville.concat(commune).concat(quartier).concat(avenue).concat(numero);

                                                bien.setAdresseBien(chaineAdresse);
                                            }
                                        }

                                        if (!aq.getBien().getComplementBienList().isEmpty()) {

                                            for (ComplementBien cbien : aq.getBien().getComplementBienList()) {

                                                switch (cbien.getTypeComplement().getComplement().getCode()) {
                                                    case GisConst.ComplementBien.BUILT_AREA: //SUPERFICIE BATIE

                                                        bien.setSuperficie_batie(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.MONTANT_LOYER: //MONTANT LOYER
                                                        bien.setMontant_loye(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.OPEN_SPACE: //SUPERIFICIE NON BATIE
                                                        bien.setSuperficie_non_batie(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.MONTANT_LOYER_ANNUEL: //MONTANT LOYER ANNUEL
                                                        bien.setMontant_annuel(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.LATTITUDE: //LATITUDE
                                                        bien.setLattitude(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.LONGITUDE: //LONGITUDE
                                                        bien.setLongitude(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                }

                                            }

                                        } else {
                                            listBien.toArray();
                                        }

                                        listBien.toArray();
                                    }
                                } else {

                                    listBien.toArray();
                                }

                            }
                        } else {

                            listBien.toArray();
                        }

                    }

                    personne.setBienList(listBien);
                    listPersonne.add(personne);
                }
                success.setPersonne(listPersonne);
            } else {
                success.setCode(GisConst.status.EMPTY_CODE);
                success.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            dataList.add(success);
            response = gson.toJson(dataList);

        } catch (Exception e) {
            success.setCode(GisConst.status.ERROR_CODE);
            success.setMessage(GisConst.status.ERROR_MESSAGE + e.getMessage());
            dataList.add(success);

            response = gson.toJson(dataList);
        }

        return response;
    }

    public static String getOwnerPropertiesAndTransactions(
            String communeId,
            String quarterId,
            String avenueId,
            String codeImmobilier,
            String impotCode,
            String periodeDate
    ) {

        String response = GeneralConst.EMPTY_STRING;

        List<MockPersonne> listPersonne = new ArrayList<>();
        List<MockBien> listBien = new ArrayList<>();
        List<AdressePersonne> adressePersonnes = new ArrayList<>();
        List<Transaction> getStatusTrans = new ArrayList<>();
        List<Success> dataList = new ArrayList<>();
        Gson gson = new Gson();
        MockPersonne personne;
        MockBien bien = new MockBien();
        Success success = new Success();

        dataAccess = new DataAccess();

        String chaineAdresse = GeneralConst.EMPTY_STRING;
        String ville = GeneralConst.EMPTY_STRING;
        String commune = GeneralConst.EMPTY_STRING;
        String quartier = GeneralConst.EMPTY_STRING;
        String avenue = GeneralConst.EMPTY_STRING;
        String numero = GeneralConst.EMPTY_STRING;

        try {

            adressePersonnes = dataAccess.getAdressePersonne(communeId, quarterId, avenueId);
            EntiteAdministrative ea;

            if (!adressePersonnes.isEmpty()) {

                success.setCode(GisConst.status.SUCCESS_CODE);
                success.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (AdressePersonne ap : adressePersonnes) {

                    personne = new MockPersonne();

                    personne.setCode(ap.getPersonne().getCode());
                    personne.setPrenom(ap.getPersonne().getPrenoms() != null ? ap.getPersonne().getPrenoms() : GeneralConst.EMPTY_STRING);
                    personne.setPostnom(ap.getPersonne().getPostnom() != null ? ap.getPersonne().getPostnom() : GeneralConst.EMPTY_STRING);
                    personne.setNom(ap.getPersonne().getNom() != null ? ap.getPersonne().getNom() : GeneralConst.EMPTY_STRING);
                    personne.setNif(ap.getPersonne().getNif() != null ? ap.getPersonne().getNif() : GeneralConst.EMPTY_STRING);

                    if (ap.getPersonne() != null) {

                        if (ap.getAdresse() != null) {

                            ea = new EntiteAdministrative();
                            ea = DataAccess.getEntiteAdministrativeByCode(
                                    ap.getAdresse().getVille());

                            if (ea != null) {
                                ville = ea.getIntitule().concat(" -->");
                            }

                            ea = DataAccess.getEntiteAdministrativeByCode(
                                    ap.getAdresse().getCommune());

                            if (ea != null) {
                                commune = " C/".concat(ea.getIntitule());
                            }

                            ea = DataAccess.getEntiteAdministrativeByCode(
                                    ap.getAdresse().getQuartier());

                            if (ea != null) {
                                quartier = " Q/".concat(ea.getIntitule());
                            }

                            ea = DataAccess.getEntiteAdministrativeByCode(
                                    ap.getAdresse().getAvenue());

                            if (ea != null) {
                                avenue = " - Av. ".concat(ea.getIntitule());
                            }

                            numero = ap.getAdresse().getNumero() != null
                                    ? " n° ".concat(ap.getAdresse().getNumero())
                                    : GeneralConst.EMPTY_STRING;

                            chaineAdresse = ville.concat(commune).concat(quartier).concat(avenue).concat(numero);

                            personne.setAdresse(chaineAdresse);
                        }

                        if (!ap.getPersonne().getAcquisitionList().isEmpty()) {

                            for (Acquisition aq : ap.getPersonne().getAcquisitionList()) {
////
                                if (aq.getBien().getTypeBien().getCode().equals(codeImmobilier)) {

                                    if (aq.getBien().getEtat()) {

                                        bien = new MockBien();

                                        personne.setIsOwner(aq.getProprietaire());

                                        bien.setCode(aq.getBien().getId());
                                        bien.setIntitule(aq.getBien().getIntitule());
                                        bien.setDescription(aq.getBien().getDescription());

                                        if (aq.getBien().getFkAdressePersonne().getParDefaut()) {

                                            if (aq.getBien().getFkAdressePersonne().getAdresse() != null) {

                                                ea = new EntiteAdministrative();
                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getVille());

                                                if (ea != null) {
                                                    ville = ea.getIntitule().concat(" -->");
                                                }

                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getCommune());

                                                if (ea != null) {
                                                    commune = " C/".concat(ea.getIntitule());
                                                }

                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getQuartier());

                                                if (ea != null) {
                                                    quartier = " Q/".concat(ea.getIntitule());
                                                }

                                                ea = DataAccess.getEntiteAdministrativeByCode(
                                                        aq.getBien().getFkAdressePersonne().getAdresse().getAvenue());

                                                if (ea != null) {
                                                    avenue = " - Av. ".concat(ea.getIntitule());
                                                }

                                                numero = aq.getBien().getFkAdressePersonne().getAdresse().getNumero() != null
                                                        ? " n° ".concat(aq.getBien().getFkAdressePersonne().getAdresse().getNumero())
                                                        : GeneralConst.EMPTY_STRING;

                                                chaineAdresse = ville.concat(commune).concat(quartier).concat(avenue).concat(numero);

                                                bien.setAdresseBien(chaineAdresse);
                                            }
                                        }

                                        if (!aq.getBien().getComplementBienList().isEmpty()) {

                                            for (ComplementBien cbien : aq.getBien().getComplementBienList()) {

                                                switch (cbien.getTypeComplement().getComplement().getCode()) {
                                                    case GisConst.ComplementBien.BUILT_AREA: //SUPERFICIE BATIE

                                                        bien.setSuperficie_batie(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.MONTANT_LOYER: //MONTANT LOYER
                                                        bien.setMontant_loye(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.OPEN_SPACE: //SUPERIFICIE NON BATIE
                                                        bien.setSuperficie_non_batie(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.MONTANT_LOYER_ANNUEL: //MONTANT LOYER ANNUEL
                                                        bien.setMontant_annuel(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.LATTITUDE: //LATITUDE
                                                        bien.setLattitude(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                    case GisConst.ComplementBien.LONGITUDE: //LONGITUDE
                                                        bien.setLongitude(cbien.getValeur() != null
                                                                ? cbien.getValeur() : GeneralConst.NumberString.ZERO);
                                                        break;
                                                }

                                            }

                                        } else {
                                            bien.setSuperficie_batie(GeneralConst.EMPTY_STRING);
                                            bien.setLongitude(GeneralConst.EMPTY_STRING);
                                            bien.setLattitude(GeneralConst.EMPTY_STRING);
                                            bien.setMontant_annuel(GeneralConst.EMPTY_STRING);
                                            bien.setMontant_loye(GeneralConst.EMPTY_STRING);
                                            bien.setSuperficie_batie(GeneralConst.EMPTY_STRING);
                                            bien.setSuperficie_non_batie(GeneralConst.EMPTY_STRING);
                                        }

                                        if (aq.getBien() != null) {
                                            Periodicite periodicite = getPeriodicite(impotCode);
                                            if (periodicite != null) {
                                                String datePart = GeneralConst.EMPTY_STRING;

                                                String datePeriode = Tools.getValidFormatDatePrint(periodeDate);
                                                switch (periodicite.getIntitule()) {
                                                    case "ANNUELLE":
                                                        datePart = "YEAR";
                                                        break;
                                                    case "MENSUELLE":
                                                        datePart = "MONTH";
                                                        break;
                                                    default:
                                                        break;
                                                }

                                                NoteCalcul nc = getNC(aq.getBien().getId(), datePeriode, datePart);
                                                if (nc != null) {
                                                    if (!nc.getNotePerceptionList().isEmpty()) {
                                                        NotePerception np = nc.getNotePerceptionList().get(0);
                                                        if (np != null) {
//
                                                            Transaction trans = new Transaction();
                                                            double solde = Double.parseDouble(np.getSolde().toString());

                                                            if (solde == 0.0) {
                                                                trans.setHasPayed(GeneralConst.NumberString.ONE);
                                                            } else {
                                                                trans.setHasPayed(GeneralConst.NumberString.ZERO);
                                                            }
                                                            getStatusTrans.add(trans);
                                                            bien.setStatus(trans.getHasPayed());

                                                        } else {
                                                            bien.setStatus(GeneralConst.NumberString.ZERO);
                                                        }
                                                    } else {
                                                        bien.setStatus(GeneralConst.NumberString.ZERO);
                                                    }
                                                } else {
                                                    bien.setStatus(GeneralConst.NumberString.ZERO);
                                                }
                                            } else {
                                                bien.setStatus(GeneralConst.NumberString.ZERO);
                                            }
                                            listBien.add(bien);
                                        } else {
                                            listBien.toArray();
                                        }
                                    }
                                } else {
                                    listBien.toArray();
                                }
                            }
                        } else {
                            listBien.toArray();
                        }
                    }

                    personne.setBienList(listBien);
                    listPersonne.add(personne);
                }
                success.setPersonne(listPersonne);
                dataList.add(success);
                response = gson.toJson(dataList);
            } else {
                success.setCode(GisConst.status.EMPTY_CODE);
                success.setMessage(GisConst.status.EMPTY_MESSAGE);
                dataList.add(success);

                response = gson.toJson(dataList);
            }
        } catch (Exception e) {
            success.setCode(GisConst.status.ERROR_CODE);
            success.setMessage(GisConst.status.ERROR_MESSAGE + e.getMessage());
            dataList.add(success);
            response = gson.toJson(dataList);
        }

        return response;
    }

    public static String getArticlesBudgetaires() {
        String response = GeneralConst.EMPTY_STRING;

        List<MockArticleBudgetaire> mab = new ArrayList<>();
        List<ArticleBudgetaire> abs = new ArrayList<>();
        List<Success> dataList = new ArrayList<>();
        Success success = new Success();
        Gson gson = new Gson();

        try {

            abs = DataAccess.getArticlesBudgetaires();

            if (!abs.isEmpty()) {

                success.setCode(GisConst.status.SUCCESS_CODE);
                success.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (ArticleBudgetaire ab : abs) {
                    MockArticleBudgetaire mab1 = new MockArticleBudgetaire();
                    List<MockCompteBancaire> cb = new ArrayList<>();
                    mab1.setTaxCode(ab.getCode());
                    mab1.setTaxName(ab.getIntitule());

                    if (!ab.getBanqueAbList().isEmpty()) {
                        for (BanqueAb banqueAb : ab.getBanqueAbList()) {

                            MockCompteBancaire mcb;

                            if (banqueAb.getFkCompte().getCode().equals("*")) {

                                List<CompteBancaire> cbs = DataAccess.getBankAccount(banqueAb.getFkBanque());
                                for (CompteBancaire compteBancaire : cbs) {
                                    mcb = new MockCompteBancaire();
                                    mcb.setBanckAccount(compteBancaire.getCode());
                                    mcb.setBanckAccountName(compteBancaire.getIntitule());
                                    cb.add(mcb);
                                }

                            } else {
                                if (banqueAb.getFkCompte().getEtat() == GeneralConst.NumberNumeric.ONE) {
                                    mcb = new MockCompteBancaire();
                                    mcb.setBanckAccount(banqueAb.getFkCompte().getCode());
                                    mcb.setBanckAccountName(banqueAb.getFkCompte().getIntitule());
                                    cb.add(mcb);

                                }
                            }
                        }
                    }

                    mab1.setBanckAccountList(cb);

                    mab.add(mab1);
                }

                success.setTaxList(mab);
            } else {
                success.setCode(GisConst.status.EMPTY_CODE);
                success.setMessage(GisConst.status.EMPTY_MESSAGE);
            }
            dataList.add(success);
            response = gson.toJson(dataList);

        } catch (Exception e) {
            success.setCode(GisConst.status.ERROR_CODE);
            success.setMessage(GisConst.status.ERROR_MESSAGE + e.getMessage());
            dataList.add(success);
            response = gson.toJson(dataList);
        }
        return response;
    }

    public static String getUnits() {
        String response = GeneralConst.EMPTY_STRING;

        List<MockUnitsObject> mockUnitsObjects = new ArrayList<>();
        List<Unite> unites = new ArrayList<>();
        List<MockUnit> mus = new ArrayList<>();
        List<Devise> ds = new ArrayList<>();
        List<MockDevise> mds = new ArrayList<>();
        MockUnitsObject muo = new MockUnitsObject();
        Gson gson = new Gson();

        try {

            unites = DataAccess.getUnits();
            ds = DataAccess.getDevise();

            if (!unites.isEmpty() || !ds.isEmpty()) {

                muo.setCode(GisConst.status.SUCCESS_CODE);
                muo.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (Unite unite : unites) {
                    MockUnit mu = new MockUnit();
                    mu.setCode(unite.getCode());
                    mu.setName(unite.getIntitule());
                    mus.add(mu);
                }

                for (Devise devise : ds) {
                    MockDevise md = new MockDevise();
                    md.setCode(devise.getCode());
                    md.setName(devise.getIntitule());
                    mds.add(md);
                }

                muo.setUnitOfMeasureList(mus);
                muo.setCurrencyList(mds);
                mockUnitsObjects.add(muo);
            } else {
                muo.setCode(GisConst.status.EMPTY_CODE);
                muo.setMessage(GisConst.status.EMPTY_MESSAGE);
            }
            response = gson.toJson(mockUnitsObjects);

        } catch (Exception e) {
            muo.setCode(GisConst.status.ERROR_CODE);
            muo.setMessage(GisConst.status.ERROR_MESSAGE + e.getMessage());
            mockUnitsObjects.add(muo);
            response = gson.toJson(mockUnitsObjects);
        }
        return response;
    }

    public static String newPersonAndProperties(String file) throws IOException, Exception {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();

        try {

            Gson gson = new Gson();
            //Type listType = new TypeToken<List<ComplementsPersonnePhysique>>() {}.getType();

            List<MockPersonCreate> mockPersonCreates = new ArrayList<>();

            JSONArray dataArray = new JSONArray(file);

            for (int i = 0; i < dataArray.length(); i++) {

                JSONObject personAndPropertiesJson = dataArray.getJSONObject(i);

                //PERSONNE
                JSONObject personneJson = personAndPropertiesJson.getJSONObject("personObject");
                PersonCreate personCreate = new PersonCreate();
                personCreate.setCode(personneJson.getString("code"));
                personCreate.setType(personneJson.getString("type"));
                personCreate.setName(personneJson.getString("name"));
                personCreate.setLastname(personneJson.getString("lastname"));
                personCreate.setFirstname(personneJson.getString("firstname"));
                personCreate.setNif(personneJson.getString("nif"));

                String adresse = personneJson.getString("address");
                personCreate.setAddress(gson.fromJson(adresse, MockAdresse.class));

                String naturalPersonComplements = personneJson.getString("naturalPersonComplementsList");
                personCreate.setNaturalPersonComplements(gson.fromJson(naturalPersonComplements, ComplementsPersonnePhysique.class));

                String legalPersonComplements = personneJson.getString("legalPersonComplementsList");
                personCreate.setLegalPersonComplements(gson.fromJson(legalPersonComplements, ComplementsPersonneMorale.class));

                //BIEN
                JSONArray jsonAdressList = personAndPropertiesJson.getJSONArray("propertyList");
                List<BienCreate> bienCreateList = new ArrayList<>();

                for (int j = 0; j < jsonAdressList.length(); j++) {

                    JSONObject propertiesJson = jsonAdressList.getJSONObject(j);

                    BienCreate bienCreate = new BienCreate();
                    bienCreate.setId(propertiesJson.getString("id"));
                    bienCreate.setName(propertiesJson.getString("name"));
                    bienCreate.setDescription(propertiesJson.getString("description"));
                    bienCreate.setDateAcquisition(propertiesJson.getString("dateAcquisition"));

                    adresse = propertiesJson.getString("address");
                    bienCreate.setAddress(gson.fromJson(adresse, MockAdresse.class));

                    String propertyComplements = propertiesJson.getString("propertyComplements");
                    bienCreate.setPropertyComplements(gson.fromJson(propertyComplements, ComplementsBienCreate.class));

                    bienCreateList.add(bienCreate);
                }

                mockPersonCreates.add(new MockPersonCreate(personCreate, bienCreateList));
            }

            response = dataAccess.savePersonWithProperties(mockPersonCreates);

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String newPerson(String file) throws IOException, Exception {

        String response = GeneralConst.EMPTY_STRING;
        responseMessage = new ResponseMessage();

        try {

            Gson gson = new Gson();

            List<PersonCreate> mockPersonCreates = new ArrayList<>();

            JSONArray dataArray = new JSONArray(file);

            for (int i = 0; i < dataArray.length(); i++) {

                JSONObject personJson = dataArray.getJSONObject(i);

                JSONObject personneJson = personJson.getJSONObject("personObject");
                PersonCreate personCreate = new PersonCreate();
                personCreate.setCode(personneJson.getString("code"));
                personCreate.setType(personneJson.getString("type"));
                personCreate.setName(personneJson.getString("name"));
                personCreate.setLastname(personneJson.getString("lastname"));
                personCreate.setFirstname(personneJson.getString("firstname"));
                personCreate.setNif(personneJson.getString("nif"));

                String adresse = personneJson.getString("address");
                personCreate.setAddress(gson.fromJson(adresse, MockAdresse.class));

                String naturalPersonComplements = personneJson.getString("naturalPersonComplementsList");
                personCreate.setNaturalPersonComplements(gson.fromJson(naturalPersonComplements, ComplementsPersonnePhysique.class));

                String legalPersonComplements = personneJson.getString("legalPersonComplementsList");
                personCreate.setLegalPersonComplements(gson.fromJson(legalPersonComplements, ComplementsPersonneMorale.class));

                mockPersonCreates.add(personCreate);
            }

            response = dataAccess.savePerson(mockPersonCreates);

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String newBien(String file) throws IOException, Exception {

        String response = GeneralConst.EMPTY_STRING;

        try {

            Gson gson = new Gson();

            List<BienCreate> mockBienCreates = new ArrayList<>();

            JSONArray dataArray = new JSONArray(file);

            for (int i = 0; i < dataArray.length(); i++) {

                JSONObject bienJson = dataArray.getJSONObject(i);
                JSONObject propertiesJson = bienJson.getJSONObject("propertyList");

                BienCreate bienCreate = new BienCreate();
                bienCreate.setId(propertiesJson.getString("id"));
                bienCreate.setName(propertiesJson.getString("name"));
                bienCreate.setDescription(propertiesJson.getString("description"));
                bienCreate.setDateAcquisition(propertiesJson.getString("dateAcquisition"));
                bienCreate.setPersonCode(propertiesJson.getString("personCode"));

                String adresse = propertiesJson.getString("address");
                bienCreate.setAddress(gson.fromJson(adresse, MockAdresse.class));

                String propertyComplements = propertiesJson.getString("propertyComplements");
                bienCreate.setPropertyComplements(gson.fromJson(propertyComplements, ComplementsBienCreate.class));

                mockBienCreates.add(bienCreate);
            }

            response = dataAccess.saveBien(mockBienCreates);

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String getTypePersonne() {
        String result = GeneralConst.EMPTY_STRING;
        List<FormeJuridique> fjs = new ArrayList<>();
        List<MockTypePersonne> mtps = new ArrayList<>();
        List<ResponseStatus> rses = new ArrayList<>();
        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        try {

            fjs = DataAccess.getTypePersonne();
            if (!fjs.isEmpty()) {

                status.setCode(GisConst.status.SUCCESS_CODE);
                status.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (FormeJuridique fj : fjs) {

                    MockTypePersonne mtp = new MockTypePersonne();
                    mtp.setCode(fj.getCode());
                    mtp.setTitle(fj.getIntitule());
                    mtps.add(mtp);

                }

                status.setPersonTypeList(mtps);
                rses.add(status);
            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            result = gson.toJson(rses);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
        }

        return result;
    }

    public static String getTypeProperty() {
        String result = GeneralConst.EMPTY_STRING;
        List<TypeBien> tbs = new ArrayList<>();
        List<MockTypeBien> mtbs = new ArrayList<>();
        List<ResponseStatus> rses = new ArrayList<>();
        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        try {

            tbs = DataAccess.getTypeBien();
            if (!tbs.isEmpty()) {

                status.setCode(GisConst.status.SUCCESS_CODE);
                status.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (TypeBien tb : tbs) {

                    MockTypeBien mtb = new MockTypeBien();
                    mtb.setPropertyTypeCode(tb.getCode());
                    mtb.setTitle(tb.getIntitule());
                    mtbs.add(mtb);

                }

                status.setPropertyTypeList(mtbs);

            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            rses.add(status);
            result = gson.toJson(rses);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            rses.add(status);
        }

        return result;
    }

    public static String getInformationPersonne(String param) {
        String result = GeneralConst.EMPTY_STRING;
        List<Personne> personnes = new ArrayList<>();
        List<MockPersonne> mps = new ArrayList<>();
        List<ResponseStatus> rses = new ArrayList<>();
        List<BienCreate> bcs = new ArrayList<>();
        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        try {

            personnes = DataAccess.getInformationPersonne(param);
            if (!personnes.isEmpty()) {

                status.setCode(GisConst.status.SUCCESS_CODE);
                status.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (Personne personne : personnes) {

                    MockPersonne mp = new MockPersonne();
                    MockTypePersonne mtp = new MockTypePersonne();
                    List<MockTypePersonne> mtps = new ArrayList<>();
                    MockEntiteAdministrative mea = new MockEntiteAdministrative();
                    EntiteAdministrative ea = new EntiteAdministrative();

                    if (!personne.getAdressePersonneList().isEmpty()) {

                        for (AdressePersonne ap : personne.getAdressePersonneList()) {

                            if (ap.getParDefaut() == true) {

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getVille());

                                if (ea != null) {
                                    mea.setCityName(ea.getIntitule().concat(" -->"));
                                }

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getCommune());

                                if (ea != null) {
                                    mea.setCommuneName(" C/".concat(ea.getIntitule()));
                                }

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getQuartier());

                                if (ea != null) {
                                    mea.setQuarterName(" Q/".concat(ea.getIntitule()));
                                }

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getAvenue());

                                if (ea != null) {
                                    mea.setAvenueName(" - Av. ".concat(ea.getIntitule()));
                                }

                                mea.setNumero(ap.getAdresse().getNumero() != null
                                        ? " n° ".concat(ap.getAdresse().getNumero())
                                        : GeneralConst.EMPTY_STRING);

                                mea.setAdresse(mea.getCityName().concat(mea.getCommuneName()).concat(mea.getQuarterName()).concat(mea.getAvenueName()).concat(mea.getNumero()));
                            }
                        }
                    }

                    if (!personne.getAcquisitionList().isEmpty()) {
                        for (Acquisition acquisition : personne.getAcquisitionList()) {

                            List<ComplementsBienCreate> cbcs = new ArrayList<>();
                            BienCreate mb = new BienCreate();
                            mb.setId(acquisition.getBien().getId());
                            mb.setName(acquisition.getBien().getIntitule());
                            mb.setDescription(acquisition.getBien().getDescription());
                            mb.setDateAcquisition(acquisition.getDateAcquisition());

                            List<ComplementBien> cbs = DataAccess.getComplementBien(acquisition.getBien().getId());
                            if (!cbs.isEmpty()) {
                                for (ComplementBien cb : cbs) {
                                    ComplementsBienCreate cbc = new ComplementsBienCreate();
                                    switch (cb.getTypeComplement().getCode()) {
                                        case "0000000002":
                                            cbc.setCB0000000002_immatriculation(cb.getValeur());
                                            break;
                                        case "0000000003":
                                            cbc.setCB0000000003_poids(cb.getValeur());
                                            break;
                                        case "0000000004":
                                            cbc.setCB0000000004_parcelBookletNumber(cb.getValeur());
                                            break;
                                        case "0000000005":
                                            cbc.setCB0000000005_referenceContrat(cb.getValeur());
                                            break;
                                        case "0000000006":
                                            cbc.setCB0000000006_builtArea(cb.getValeur());
                                            break;
                                        case "0000000007":
                                            cbc.setCB0000000007_openSpaceArea(cb.getValeur());
                                            break;
                                        case "0000000031":
                                            cbc.setCB0000000031_rentAmount(cb.getValeur());
                                            break;
                                        case "0000000033":
                                            cbc.setCB0000000033_annualRentAmount(cb.getValeur());
                                            break;
                                        case "0000000098":
                                            cbc.setCB0000000098_latitude(cb.getValeur());
                                            break;
                                        case "0000000099":
                                            cbc.setCB0000000099_longitude(cb.getValeur());
                                            break;
                                    }
                                    cbcs.add(cbc);
                                }
                            }
                            mb.setPropertyComplement(cbcs);
                            bcs.add(mb);
                        }
                    }

                    mp.setCode(personne.getCode());
                    mp.setNom(personne.getNom());
                    mp.setPostnom(personne.getPostnom());
                    mp.setPrenom(personne.getPrenoms());
                    mtp.setTitle(personne.getFormeJuridique().getIntitule());

                    mtps.add(mtp);
                    mp.setPersonType(mtp.getTitle());
                    mp.setAdresse(mea.getAdresse());
                    mp.setPropertyList(bcs);
                    mps.add(mp);
                }

                status.setPersonList(mps);

            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            rses.add(status);
            result = gson.toJson(rses);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            rses.add(status);
        }

        return result;
    }

    public static String getPersonneById(String id) {
        String result = GeneralConst.EMPTY_STRING;
        List<Personne> personnes = new ArrayList<>();
        List<MockPersonne> mps = new ArrayList<>();
        List<ResponseStatus> rses = new ArrayList<>();
        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        try {

            personnes = DataAccess.getPersonneById(id);
            if (!personnes.isEmpty()) {

                status.setCode(GisConst.status.SUCCESS_CODE);
                status.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (Personne personne : personnes) {

                    MockPersonne mp = new MockPersonne();
                    MockTypePersonne mtp = new MockTypePersonne();
                    List<MockTypePersonne> mtps = new ArrayList<>();
                    MockEntiteAdministrative mea = new MockEntiteAdministrative();
                    EntiteAdministrative ea = new EntiteAdministrative();

                    if (!personne.getAdressePersonneList().isEmpty()) {

                        for (AdressePersonne ap : personne.getAdressePersonneList()) {

                            if (ap.getParDefaut() == true) {

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getVille());

                                if (ea != null) {
                                    mea.setCityName(ea.getIntitule().concat(" -->"));
                                }

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getCommune());

                                if (ea != null) {
                                    mea.setCommuneName(" C/".concat(ea.getIntitule()));
                                }

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getQuartier());

                                if (ea != null) {
                                    mea.setQuarterName(" Q/".concat(ea.getIntitule()));
                                }

                                ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getAvenue());

                                if (ea != null) {
                                    mea.setAvenueName(" - Av. ".concat(ea.getIntitule()));
                                }

                                mea.setNumero(ap.getAdresse().getNumero() != null
                                        ? " n° ".concat(ap.getAdresse().getNumero())
                                        : GeneralConst.EMPTY_STRING);

                                mea.setAdresse(mea.getCityName().concat(mea.getCommuneName()).concat(mea.getQuarterName()).concat(mea.getAvenueName()).concat(mea.getNumero()));
                            }
                        }
                    }

                    mp.setCode(personne.getCode());
                    mp.setNom(personne.getNom());
                    mp.setPostnom(personne.getPostnom());
                    mp.setPrenom(personne.getPrenoms());

                    if (personne.getLoginWeb() != null) {
                        mp.setPhoneNumber(personne.getLoginWeb().getTelephone());
                        mp.setMailAddress(personne.getLoginWeb().getMail());
                    } else {
                        mp.setPhoneNumber(GeneralConst.EMPTY_STRING);
                        mp.setMailAddress(GeneralConst.EMPTY_STRING);
                    }

                    mtp.setTitle(personne.getFormeJuridique().getIntitule());
                    mtps.add(mtp);
                    mp.setPersonType(mtp.getTitle());
                    mp.setAdresse(mea.getAdresse());
                    mps.add(mp);
                }

                status.setPersonObject(mps);

            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            rses.add(status);
            result = gson.toJson(rses);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            rses.add(status);
        }

        return result;
    }

    public static String newRetraitDeclaration(String file) throws IOException, Exception {

        String response = GeneralConst.EMPTY_STRING;

        try {

            List<MockRetraitDeclaration> mockRetraitDeclaration = new ArrayList<>();

            JSONArray dataArray = new JSONArray(file);

            for (int i = 0; i < dataArray.length(); i++) {

                JSONObject retraitDeclarationJson = dataArray.getJSONObject(i);
                JSONObject propertiesJson = retraitDeclarationJson.getJSONObject("declarationObject");

                MockRetraitDeclaration retraitDecl = new MockRetraitDeclaration();

                retraitDecl.setRequerant(propertiesJson.getString("requerant"));
                retraitDecl.setFkAssujetti(propertiesJson.getString("fkAssujetti"));
                //retraitDecl.setCodeDeclaration(propertiesJson.getString("codeDeclaration"));
                retraitDecl.setFkArticleBudgetaire(propertiesJson.getString("fkArticleBudgetaire"));
                retraitDecl.setFkPeriode(propertiesJson.getString("fkPeriode"));
                retraitDecl.setFkAgent(propertiesJson.getString("fkAgent"));
                retraitDecl.setValeurBase(new BigDecimal(propertiesJson.getDouble("valeurBase")));
                retraitDecl.setAmount(new BigDecimal(propertiesJson.getDouble("amount")));
                retraitDecl.setDevise(propertiesJson.getString("devise"));

                mockRetraitDeclaration.add(retraitDecl);
            }

            response = dataAccess.saveRetraitDeclaration(mockRetraitDeclaration);

        } catch (Exception e) {
            throw e;
        }

        return response;
    }

    public static String getTransactions(String agentId) {

        String result = GeneralConst.EMPTY_STRING;
        List<Journal> journals = new ArrayList<>();
        List<Transaction> trs = new ArrayList<>();
        List<ResponseStatus> rses = new ArrayList<>();
        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        try {

            journals = DataAccess.getTransactions(agentId);
            if (!journals.isEmpty()) {

                status.setCode(GisConst.status.SUCCESS_CODE);
                status.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (Journal journal : journals) {

                    Transaction tr = new Transaction();
                    tr.setTransactionId(journal.getCode());
                    tr.setTransactionDate(DateUtil.formatToDayMonthYear(journal.getDateCreat()));
                    tr.setAmountPayed(journal.getMontant().toString());
                    tr.setCurrency(journal.getDevise().getCode());
                    tr.setDocumentType(journal.getTypeDocument());
                    tr.setCustomer(journal.getPersonne().toString());
                    tr.setCustomerType(journal.getPersonne().getFormeJuridique().getIntitule());
                    trs.add(tr);
                }

                status.setTransactionObject(trs);
            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            rses.add(status);
            result = gson.toJson(rses);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            rses.add(status);
        }

        return result;
    }

    public static String getRates(String codeAb) {

        String result = GeneralConst.EMPTY_STRING;
        List<Tarif> tarifs = new ArrayList<>();
        List<MockTarif> mts = new ArrayList<>();
        List<ResponseStatus> rses = new ArrayList<>();
        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        try {

            tarifs = DataAccess.getRates(codeAb);
            if (!tarifs.isEmpty()) {

                status.setCode(GisConst.status.SUCCESS_CODE);
                status.setMessage(GisConst.status.SUCCESS_MESSAGE);

                for (Tarif tarif : tarifs) {

                    MockTarif mt = new MockTarif();
                    mt.setRateCode(tarif.getCode());
                    mt.setRateName(tarif.getIntitule());
                    mts.add(mt);
                }

                status.setRateObject(mts);
            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
            }

            rses.add(status);
            result = gson.toJson(rses);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            rses.add(status);
        }

        return result;
    }

    public static String getDetailsByLocationIdObjet(String locationId) {

        String result = GeneralConst.EMPTY_STRING;
        Personne personne = new Personne();
        MockPersonneLocation mpl = new MockPersonneLocation();
        ResponseStatus status = new ResponseStatus();
        ComplementBien cb = new ComplementBien();
        Gson gson = new Gson();

        try {

            cb = DataAccess.getDetailsByLocationId(locationId);
            if (cb != null) {
                if (!cb.getBien().getAcquisitionList().isEmpty()) {
                    personne = DataAccess.getPersonById(cb.getBien().getAcquisitionList().get(0).getPersonne().getCode());
                    if (personne != null) {
                        EntiteAdministrative ea = new EntiteAdministrative();
                        MockEntiteAdministrative mea = new MockEntiteAdministrative();
                        MockPersonne mp = new MockPersonne();
                        mp.setNif(personne.getNif() != null ? personne.getNif() : GeneralConst.EMPTY_STRING);
                        mp.setNom(personne.getNom() != null ? personne.getNom() : GeneralConst.EMPTY_STRING);
                        mp.setPostnom(personne.getPostnom() != null ? personne.getPostnom() : GeneralConst.EMPTY_STRING);
                        mp.setPrenom(personne.getPrenoms() != null ? personne.getPrenoms() : GeneralConst.EMPTY_STRING);
                        mpl.setPersonObject(mp);

                        if (!personne.getAdressePersonneList().isEmpty()) {

                            for (AdressePersonne ap : personne.getAdressePersonneList()) {

                                if (ap.getParDefaut() == true) {

                                    ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getVille());

                                    if (ea != null) {
                                        mea.setCityName(ea.getIntitule().concat(" -->"));
                                    }

                                    ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getCommune());

                                    if (ea != null) {
                                        mea.setCommuneName(" C/".concat(ea.getIntitule()));
                                    }

                                    ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getQuartier());

                                    if (ea != null) {
                                        mea.setQuarterName(" Q/".concat(ea.getIntitule()));
                                    }

                                    ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getAvenue());

                                    if (ea != null) {
                                        mea.setAvenueName(" - Av. ".concat(ea.getIntitule()));
                                    }

                                    mea.setNumero(ap.getAdresse().getNumero() != null
                                            ? " n° ".concat(ap.getAdresse().getNumero())
                                            : GeneralConst.EMPTY_STRING);

                                    mea.setAdresse(mea.getCityName().concat(mea.getCommuneName()).concat(mea.getQuarterName()).concat(mea.getAvenueName()).concat(mea.getNumero()));
                                }
                            }
                        }

                        mpl.setAddress(mea.getAdresse());
                    }

                    NoteCalcul nc = DataAccess.getNC(cb.getBien().getId());
                    if (nc != null) {
                        Tarif tarifs = DataAccess.getTarif(nc.getDetailsNcList().get(0).getArticleBudgetaire());

                        if (nc.getDetailsNcList().get(0).getPeriodeDeclaration() != null) {
                            mpl.setPeriod(Tools.getPeriodeIntitule(nc.getDetailsNcList().get(0).getPeriodeDeclaration().getDebut(),
                                    nc.getDetailsNcList().get(0).getArticleBudgetaire().getPeriodicite().getNbrJour().toString()));
                        } else {
                            mpl.setPeriod(GeneralConst.EMPTY_STRING);
                        }

                        if (tarifs != null) {
                            mpl.setRate(tarifs.getIntitule());
                        }

                        NotePerception np = DataAccess.getNP(nc.getNumero());
                        if (np != null) {
                            mpl.setAmountToPay(np.getNetAPayer().toString());
                            mpl.setAmountPayed(np.getSolde().toString());
                        }
                    } else {
                        mpl.setRate(GeneralConst.EMPTY_STRING);
                        mpl.setAmountToPay("0.00");
                        mpl.setAmountPayed("0.00");
                        mpl.setPeriod("0");
                    }

                    List<ComplementBien> cbs = DataAccess.getComplementBien(cb.getBien().getId());
                    if (!cbs.isEmpty()) {
                        for (ComplementBien cb1 : cbs) {
                            switch (cb1.getTypeComplement().getCode()) {
                                case "0000000006"://superficie batie
                                    mpl.setBuiltArea(cb1.getValeur());
                                    break;
                                case "0000000007"://superficie non batie
                                    mpl.setOpenSpace(cb1.getValeur());
                                    break;
                                case "0000000104"://nombre d'appartement
                                    mpl.setNumberOfAppartment(cb1.getValeur());
                                    break;
                                case "0000000103"://nombre d'etage
                                    mpl.setNumberOfStage(cb1.getValeur());
                                    break;
                            }
                        }

                    } else {
                        mpl.setBuiltArea(GeneralConst.EMPTY_STRING);
                        mpl.setOpenSpace(GeneralConst.EMPTY_STRING);
                        mpl.setNumberOfAppartment(GeneralConst.EMPTY_STRING);
                        mpl.setNumberOfStage(GeneralConst.EMPTY_STRING);
                    }
                }
                result = gson.toJson(mpl);
            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
                result = gson.toJson(status);
            }

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            result = gson.toJson(status);
        }

        return result;
    }

    public static String getDetailsByLocationId(String locationId, String typeComplementBien, String typeBien) throws IOException {

        String result = GeneralConst.EMPTY_STRING;

        ResponseStatus status = new ResponseStatus();
        List<MockPersonneLocation> mpls = new ArrayList<>();
        List<ComplementBien> cbs = new ArrayList<>();
        List<MockPersonne> mps = new ArrayList<>();
        MockDetailProperty mdp = new MockDetailProperty();
        Gson gson = new Gson();

        propertiesConfig = new PropertiesConfig();

        String superficieBatie = propertiesConfig.getContent(PropertiesConst.Config.BUILT_AREA);
        String superficieNonBatie = propertiesConfig.getContent(PropertiesConst.Config.SUPERFICIE_NON_BATIE);
        String nombreAppartement = propertiesConfig.getContent(PropertiesConst.Config.NOMBRE_APPARTEMENT);
        String nombreEtage = propertiesConfig.getContent(PropertiesConst.Config.NOMBRE_ETAGE);

        try {

            cbs = DataAccess.getDetailsByLocation(locationId, typeComplementBien, typeBien);
            if (!cbs.isEmpty()) {
                mdp.setLocationId(locationId);

                for (ComplementBien cb : cbs) {

                    List<Personne> personnes = new ArrayList<>();

                    if (!cb.getBien().getAcquisitionList().isEmpty()) {

                        personnes = DataAccess.getPersonsById(cb.getBien().getAcquisitionList().get(0).getPersonne().getCode());

                        if (!personnes.isEmpty()) {

                            for (Personne personne : personnes) {

                                EntiteAdministrative ea = new EntiteAdministrative();
                                MockEntiteAdministrative mea = new MockEntiteAdministrative();
                                MockPersonne mp = new MockPersonne();
                                MockPersonneLocation mpl = new MockPersonneLocation();

                                mp.setNif(personne.getNif() != null ? personne.getNif() : GeneralConst.EMPTY_STRING);
                                mp.setNom(personne.getNom() != null ? personne.getNom() : GeneralConst.EMPTY_STRING);
                                mp.setPostnom(personne.getPostnom() != null ? personne.getPostnom() : GeneralConst.EMPTY_STRING);
                                mp.setPrenom(personne.getPrenoms() != null ? personne.getPrenoms() : GeneralConst.EMPTY_STRING);
                                mpl.setPersonObject(mp);

                                if (!personne.getAdressePersonneList().isEmpty()) {

                                    for (AdressePersonne ap : personne.getAdressePersonneList()) {

                                        if (ap.getParDefaut() == true) {

                                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getVille());

                                            if (ea != null) {
                                                mea.setCityName(ea.getIntitule().concat(" -->"));
                                            }

                                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getCommune());

                                            if (ea != null) {
                                                mea.setCommuneName(" C/".concat(ea.getIntitule()));
                                            }

                                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getQuartier());

                                            if (ea != null) {
                                                mea.setQuarterName(" Q/".concat(ea.getIntitule()));
                                            }

                                            ea = DataAccess.getEntiteAdministrativeByCode(ap.getAdresse().getAvenue());

                                            if (ea != null) {
                                                mea.setAvenueName(" - Av. ".concat(ea.getIntitule()));
                                            }

                                            mea.setNumero(ap.getAdresse().getNumero() != null
                                                    ? " n° ".concat(ap.getAdresse().getNumero())
                                                    : GeneralConst.EMPTY_STRING);

                                            mea.setAdresse(mea.getCityName().concat(mea.getCommuneName()).concat(mea.getQuarterName()).concat(mea.getAvenueName()).concat(mea.getNumero()));
                                        }
                                    }
                                }

                                mpl.setAddress(mea.getAdresse());

                                NoteCalcul nc = DataAccess.getNC(cb.getBien().getId());
                                if (nc != null) {
                                    Tarif tarifs = DataAccess.getTarif(nc.getDetailsNcList().get(0).getArticleBudgetaire());

                                    if (nc.getDetailsNcList().get(0).getPeriodeDeclaration() != null) {
                                        mpl.setPeriod(Tools.getPeriodeIntitule(nc.getDetailsNcList().get(0).getPeriodeDeclaration().getDebut(),
                                                nc.getDetailsNcList().get(0).getArticleBudgetaire().getPeriodicite().getNbrJour().toString()));
                                    } else {
                                        mpl.setPeriod(GeneralConst.EMPTY_STRING);
                                    }

                                    if (tarifs != null) {
                                        mpl.setRate(tarifs.getIntitule());
                                    }

                                    NotePerception np = DataAccess.getNP(nc.getNumero());
                                    if (np != null) {
                                        mpl.setAmountToPay(np.getNetAPayer().toString());
                                        mpl.setAmountPayed(np.getSolde().toString());
                                    }
                                } else {
                                    mpl.setRate(GeneralConst.EMPTY_STRING);
                                    mpl.setAmountToPay("0.00");
                                    mpl.setAmountPayed("0.00");
                                    mpl.setPeriod("0");
                                }

                                cbs = DataAccess.getComplementBien(cb.getBien().getId());
                                if (!cbs.isEmpty()) {

                                    for (ComplementBien cb1 : cbs) {
                                        if (cb1.getTypeComplement().getCode().equals(superficieBatie)) {
                                            mpl.setBuiltArea(cb1.getValeur());
                                        } else if (cb1.getTypeComplement().getCode().equals(superficieNonBatie)) {
                                            mpl.setOpenSpace(cb1.getValeur());
                                        } else if (cb1.getTypeComplement().getCode().equals(nombreAppartement)) {
                                            mpl.setNumberOfAppartment(cb1.getValeur());
                                        } else if (cb1.getTypeComplement().getCode().equals(nombreEtage)) {
                                            mpl.setNumberOfStage(cb1.getValeur());
                                        }
                                    }

                                } else {
                                    mpl.setBuiltArea(GeneralConst.EMPTY_STRING);
                                    mpl.setOpenSpace(GeneralConst.EMPTY_STRING);
                                    mpl.setNumberOfAppartment(GeneralConst.EMPTY_STRING);
                                    mpl.setNumberOfStage(GeneralConst.EMPTY_STRING);
                                }

                                mpls.add(mpl);

                            }
                        }

                    }
                    mdp.setProperiesList(mpls);
                }
                result = gson.toJson(mdp);

            } else {
                status.setCode(GisConst.status.EMPTY_CODE);
                status.setMessage(GisConst.status.EMPTY_MESSAGE);
                result = gson.toJson(status);
            }

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            result = gson.toJson(status);
        }

        return result;
    }

    public static String getAllLocationPayementData(String typeComplementBien, String typeBien) throws IOException {

        String result;
        List<Bien> biens;
        List<LocationPaymentData> locationPaymentDatas = new ArrayList<>();
        List<NotePerception> nps;

        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        propertiesConfig = new PropertiesConfig();

        String locationId = propertiesConfig.getContent(PropertiesConst.Config.TYPE_COMPLEMENT_BIEN_LOCATION_ID);

        try {

            biens = DataAccess.getBien(locationId);

            for (Bien bien : biens) {

                String locationID = GeneralConst.EMPTY_STRING;

                for (ComplementBien cb : bien.getComplementBienList()) {

                    if (cb.getTypeComplement().getCode().equals(locationId)) {

                        locationID = cb.getValeur();
                        break;
                    }

                }

                if (!locationID.isEmpty()) {

                    nps = DataAccess.getNotePerception(bien.getId());

                    for (NotePerception np : nps) {

                        LocationPaymentData locationPaymentData = new LocationPaymentData();
                        locationPaymentData.setLocationId(locationID);
                        locationPaymentData.setUID(np.getNoteCalcul().getNumero());

                        if (np.getDetailsNcList().get(0).getArticleBudgetaire() != null) {
                            locationPaymentData.setTaxeName(np.getDetailsNcList().get(0).getArticleBudgetaire().getIntitule());
                        }

                        if (np.getPayer().doubleValue() > 0) {
                            locationPaymentData.setPaymentStatus("Payed");
                        } else {
                            locationPaymentData.setPaymentStatus("Not payed");
                        }

                        if (np.getDetailsNcList().get(0).getPeriodeDeclaration() != null) {
                            locationPaymentData.setPeriod(Tools.getPeriodeIntitule(np.getDetailsNcList().get(0).getPeriodeDeclaration().getDebut(),
                                    np.getDetailsNcList().get(0).getArticleBudgetaire().getPeriodicite().getNbrJour().toString()));
                        } else {
                            locationPaymentData.setPeriod(GeneralConst.EMPTY_STRING);
                        }

                        locationPaymentDatas.add(locationPaymentData);
                    }

                }

            }

            result = gson.toJson(locationPaymentDatas);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            result = gson.toJson(status);
        }

        return result;
    }

    public static String getAllLocationData(String typeComplementBien, String typeBien) throws IOException {

        String result;
        List<Bien> biens;
        List<MockDashboardData> mdds = new ArrayList<>();

        ResponseStatus status = new ResponseStatus();
        Gson gson = new Gson();

        propertiesConfig = new PropertiesConfig();

        String locationId = propertiesConfig.getContent(PropertiesConst.Config.TYPE_COMPLEMENT_BIEN_LOCATION_ID);
        String typeMaison = propertiesConfig.getContent(PropertiesConst.Config.TYPE_MAISON);

        try {

            biens = DataAccess.getBien(locationId);

            for (Bien bien : biens) {

                MockDashboardData mdd = new MockDashboardData();

                for (ComplementBien cb : bien.getComplementBienList()) {

                    if (cb.getTypeComplement().getCode().equals(locationId)) {
                        mdd.setLocationId(cb.getValeur());
                    } else if (cb.getTypeComplement().getCode().equals(typeMaison)) {
                        mdd.setType(cb.getValeur());
                    }

                }

                mdds.add(mdd);

            }

            result = gson.toJson(mdds);

        } catch (Exception e) {
            status.setCode(GisConst.status.ERROR_CODE);
            status.setMessage(GisConst.status.ERROR_MESSAGE);
            result = gson.toJson(status);
        }

        return result;
    }
}
