/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.business;

import cd.hologram.apidgrk.entities.Amr;
import cd.hologram.apidgrk.entities.ArticleBudgetaire;
import cd.hologram.apidgrk.entities.BanqueAb;
import cd.hologram.apidgrk.entities.BonAPayer;
import cd.hologram.apidgrk.entities.Bordereau;
import cd.hologram.apidgrk.entities.CompteBancaire;
import cd.hologram.apidgrk.entities.Journal;
import cd.hologram.apidgrk.entities.NotePerception;
import cd.hologram.apidgrk.entities.Palier;
import cd.hologram.apidgrk.entities.PeriodeDeclaration;
import cd.hologram.apidgrk.entities.Personne;
import cd.hologram.apidgrk.entities.RetraitDeclaration;
import cd.hologram.apidgrk.entities.Service;
import java.util.List;

/**
 *
 * @author moussa.toure
 */
public interface IDataAccess {
    
    public NotePerception getNotePerceptionByNumero(String numero);
    public Amr getAMRByNumero(String numero);
    public Personne getPersonneByCode(String code);
    public ArticleBudgetaire getArticleBudgetairebyCode(String code);
    public Service getServiceByCode(String code);
    public CompteBancaire getCompteBancaireByCode(String code);
    public BanqueAb getAbByCompteAb(String Ab,String compte);
    public List<RetraitDeclaration> getRetraitDeclarationByCode(String codeDeclaration);
    public PeriodeDeclaration getPeriodeDeclaration(int code);
    public List<PeriodeDeclaration> getPeriodeByAssujettissement(String assu);
    public Palier getPalierByTarif(String tarif, String articleBudgetaire);
    public BonAPayer getBonPayerByCode(String code);
    public Bordereau getBordereauByNumero(String numero,boolean isDeclaration);
    public List<Journal> getJournal(String reference);
    public List<Bordereau> getBordereau(String reference);
    public List<Palier> getTarifByArticle(String ab);
    public List<Service> getServiceAssiete();
    public List<ArticleBudgetaire> articleBudgetaires(String service);
}
