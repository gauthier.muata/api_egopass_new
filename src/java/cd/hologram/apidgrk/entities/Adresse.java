/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package cd.hologram.apidgrk.entities;

import cd.hologram.apidgrk.business.DataAccess;
import cd.hologram.apidgrk.constants.GeneralConst;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
*
* @author moussa.toure
*/
@Entity
@Cacheable(false)
@Table(name = "T_ADRESSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adresse.findAll", query = "SELECT a FROM Adresse a"),
    @NamedQuery(name = "Adresse.findById", query = "SELECT a FROM Adresse a WHERE a.id = :id"),
    @NamedQuery(name = "Adresse.findByProvince", query = "SELECT a FROM Adresse a WHERE a.province = :province"),
    @NamedQuery(name = "Adresse.findByDistrict", query = "SELECT a FROM Adresse a WHERE a.district = :district"),
    @NamedQuery(name = "Adresse.findByVille", query = "SELECT a FROM Adresse a WHERE a.ville = :ville"),
    @NamedQuery(name = "Adresse.findByCommune", query = "SELECT a FROM Adresse a WHERE a.commune = :commune"),
    @NamedQuery(name = "Adresse.findByQuartier", query = "SELECT a FROM Adresse a WHERE a.quartier = :quartier"),
    @NamedQuery(name = "Adresse.findByAvenue", query = "SELECT a FROM Adresse a WHERE a.avenue = :avenue"),
    @NamedQuery(name = "Adresse.findByNumero", query = "SELECT a FROM Adresse a WHERE a.numero = :numero"),
    @NamedQuery(name = "Adresse.findByEtat", query = "SELECT a FROM Adresse a WHERE a.etat = :etat")})
public class Adresse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 25)
    @Column(name = "PROVINCE")
    private String province;
    @Size(max = 25)
    @Column(name = "DISTRICT")
    private String district;
    @Size(max = 25)
    @Column(name = "VILLE")
    private String ville;
    @Size(max = 25)
    @Column(name = "COMMUNE")
    private String commune;
    @Size(max = 25)
    @Column(name = "QUARTIER")
    private String quartier;
    @Size(max = 25)
    @Column(name = "AVENUE")
    private String avenue;
    @Size(max = 20)
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "ETAT")
    private Boolean etat;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresse")
    private List<AdressePersonne> adressePersonneList;
    @OneToMany(mappedBy = "adresse")
    private List<Site> siteList;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "Chaine")
    private String chaine;

    public Adresse() {
    }

    public Adresse(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
       this.ville = ville;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getChaine() {
        return chaine;
    }

    public void setChaine(String chaine) {
        this.chaine = chaine;
    }

    @XmlTransient
    public List<AdressePersonne> getAdressePersonneList() {
        return adressePersonneList;
    }

    public void setAdressePersonneList(List<AdressePersonne> adressePersonneList) {
        this.adressePersonneList = adressePersonneList;
    }

    @XmlTransient
    public List<Site> getSiteList() {
        return siteList;
    }

    public void setSiteList(List<Site> siteList) {
        this.siteList = siteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Adresse)) {
            return false;
        }
        Adresse other = (Adresse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {

        if (chaine == null) {

            EntiteAdministrative eaVille = ville != null ? DataAccess.getEntiteAdministrativeByCode(ville) : null;
            EntiteAdministrative eaCommune = commune != null ? DataAccess.getEntiteAdministrativeByCode(commune) : null;
            EntiteAdministrative eaQuartier = quartier != null ? DataAccess.getEntiteAdministrativeByCode(quartier) : null;
            EntiteAdministrative eaAvenue = avenue != null ? DataAccess.getEntiteAdministrativeByCode(avenue) : null;

            String myVille = eaVille != null ? eaVille.getIntitule() : GeneralConst.EMPTY_STRING;
            String myCommune = eaCommune != null ? eaCommune.getIntitule() : GeneralConst.EMPTY_STRING;
            String myQuartier = eaQuartier != null ? eaQuartier.getIntitule() : GeneralConst.EMPTY_STRING;
            String myAvenue = eaAvenue != null ? eaAvenue.getIntitule() : GeneralConst.EMPTY_STRING;

            String myNumero = numero != null ? numero : GeneralConst.EMPTY_STRING;

            return myVille + "--> C/" + myCommune
                    + " - Q/" + myQuartier + " - Av. " + myAvenue + " n° " + myNumero;

        } else {
            
            EntiteAdministrative eaVille = ville != null ? DataAccess.getEntiteAdministrativeByCode(ville) : null;
            String myVille = eaVille != null ? eaVille.getIntitule() : GeneralConst.EMPTY_STRING;
            
            return myVille + "--> " + chaine;
        }

    }

}
