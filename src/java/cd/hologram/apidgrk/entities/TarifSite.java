/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrateur
 */
@Entity
@Cacheable(false)
@Table(name = "T_TARIF_SITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TarifSite.findAll", query = "SELECT t FROM TarifSite t"),
    @NamedQuery(name = "TarifSite.findById", query = "SELECT t FROM TarifSite t WHERE t.id = :id"),
    @NamedQuery(name = "TarifSite.findByFkDevise", query = "SELECT t FROM TarifSite t WHERE t.fkDevise = :fkDevise"),
//    @NamedQuery(name = "TarifSite.findByFkSiteDestination", query = "SELECT t FROM TarifSite t WHERE t.fkSiteDestination = :fkSiteDestination"),
//    @NamedQuery(name = "TarifSite.findByFkSiteProvenance", query = "SELECT t FROM TarifSite t WHERE t.fkSiteProvenance = :fkSiteProvenance"),
    @NamedQuery(name = "TarifSite.findByTaux", query = "SELECT t FROM TarifSite t WHERE t.taux = :taux"),
    @NamedQuery(name = "TarifSite.findByEtat", query = "SELECT t FROM TarifSite t WHERE t.etat = :etat"),
    @NamedQuery(name = "TarifSite.findByAgentCreate", query = "SELECT t FROM TarifSite t WHERE t.agentCreate = :agentCreate"),
    @NamedQuery(name = "TarifSite.findByDateCreate", query = "SELECT t FROM TarifSite t WHERE t.dateCreate = :dateCreate"),
    @NamedQuery(name = "TarifSite.findByAgentMaj", query = "SELECT t FROM TarifSite t WHERE t.agentMaj = :agentMaj"),
    @NamedQuery(name = "TarifSite.findByDateMaj", query = "SELECT t FROM TarifSite t WHERE t.dateMaj = :dateMaj")})
public class TarifSite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
//    @Size(max = 50)
//    @Column(name = "FK_SITE_PROVENANCE")
//    private String fkSiteProvenance;
//    @Size(max = 50)
//    @Column(name = "FK_SITE_DESTINATION")
//    private String fkSiteDestination;
    @Size(max = 10)
    @Column(name = "FK_DEVISE")
    private String fkDevise;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @JoinColumn(name = "FK_TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif fkTarif;

    public TarifSite() {
    }

    public TarifSite(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public String getFkSiteProvenance() {
//        return fkSiteProvenance;
//    }
//
//    public void setFkSiteProvenance(String fkSiteProvenance) {
//        this.fkSiteProvenance = fkSiteProvenance;
//    }
//
//    public String getFkSiteDestination() {
//        return fkSiteDestination;
//    }
//
//    public void setFkSiteDestination(String fkSiteDestination) {
//        this.fkSiteDestination = fkSiteDestination;
//    }
    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Tarif getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(Tarif fkTarif) {
        this.fkTarif = fkTarif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TarifSite)) {
            return false;
        }
        TarifSite other = (TarifSite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entites.TarifSite[ id=" + id + " ]";
    }

}
