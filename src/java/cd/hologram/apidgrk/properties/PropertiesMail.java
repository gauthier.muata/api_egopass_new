/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.apidgrk.properties;

import cd.hologram.apidgrk.constants.PropertiesConst;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author didierson.amuri
 */
public class PropertiesMail {
    
    private final Properties propertiesMail = new Properties();

    public PropertiesMail() throws IOException {
        propertiesMail.load(getClass().getResourceAsStream(PropertiesConst.Mail.PROPERTIES_FILE_PATH));
    }
    
    public String getContent(String title) {
        return propertiesMail.getProperty(title);
    }
    
}
